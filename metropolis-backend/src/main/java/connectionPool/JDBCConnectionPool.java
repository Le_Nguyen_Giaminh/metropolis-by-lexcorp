package connectionPool;

import org.postgresql.Driver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public class JDBCConnectionPool {
    private ArrayList<Connection> pool;
    private int maxConnection;
    private final static Logger connectionLog = LoggerFactory.getLogger(JDBCConnectionPool.class.getName());
    ConnectionProps props = ConnectionProps.getInstance("connectionProps.properties");

    public JDBCConnectionPool(int n) {
        pool = new ArrayList<>();
        maxConnection = n;
    }

    public void createConnection() {
        try {
            Class< ? extends Driver> driver = (Class<Driver>) Class.forName(props.getDriverClassName());
            for(int i=0; i<maxConnection; i++) {
                Connection connection = DriverManager.getConnection(props.getUrl(), props.getUsername(),props.getPassword());
                pool.add(connection);
            }
            connectionLog.info("" + maxConnection + " database connections are created");
        } catch (SQLException | ClassNotFoundException e) {
            connectionLog.info(e.getMessage());
        }
    }

    public synchronized Connection getConnection() {
        if(pool.size()==0) {
            return null;
        }
        else {
            Connection oneConnection = pool.get(0);
            pool.remove(0);
            return oneConnection;
        }
    }

    public void addConnection(Connection c) {
        pool.add(c);
    }
    
    public void closeAll() throws SQLException {
        for(Connection connect : pool) {
            if(connect!=null) {connect.close();}
        }
    }
}
