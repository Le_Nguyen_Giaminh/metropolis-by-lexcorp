package connectionPool;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class ConnectionProps {

    private static ConnectionProps instance = null;
    private String driverClassName, url, username, password;

    private ConnectionProps(String filename) {
        try {
            // extract the information from the .properties file
            Properties props = new Properties();
            InputStream inStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
            props.load(inStream);
            driverClassName = props.getProperty("database.driverClassName");
            url = props.getProperty("database.url");
            username = props.getProperty("database.username");
            password = props.getProperty("database.password");
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public static ConnectionProps getInstance(String filename) {
        ConnectionProps res = instance;
        if (res != null) {
            return res;
        }
        // using synchronized to prevent multiple threads problem
        synchronized(ConnectionProps.class) {
            if (instance == null) {
                instance = new ConnectionProps(filename);
            }
            return instance;
        }
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
