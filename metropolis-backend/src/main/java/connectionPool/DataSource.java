package connectionPool;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {
    private static JDBCConnectionPool conn;

    public DataSource(JDBCConnectionPool conn) {
        this.conn = conn;
    }

    public static Connection getConnection() {
        return conn.getConnection();
    }

    public static void addConnection(Connection c) {
        conn.addConnection(c);
    }

    public static void closeConnection() throws SQLException {
        conn.closeAll();
    }
}