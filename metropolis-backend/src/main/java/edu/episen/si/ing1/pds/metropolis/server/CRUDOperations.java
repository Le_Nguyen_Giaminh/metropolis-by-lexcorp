package edu.episen.si.ing1.pds.metropolis.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.*;
import java.util.Map;

public class CRUDOperations {

    private Connection conn;

    private static Logger crudLog = LoggerFactory.getLogger(CRUDOperations.class.getName());

    public CRUDOperations(Connection conn) {
        this.conn = conn;
    }

    public String executeRequest(String rq) {
        String res = "";
        try {
            String[] s = rq.split(" ");
            if(s[0].equals("select")) {
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(rq);
                while (rs.next()) {
                    String sub = "";
                    int col_nb = rs.getMetaData().getColumnCount();
                    for (int i = 0; i < col_nb; i++) {
                        sub += "" + rs.getMetaData().getColumnLabel(i+1) + "=" + rs.getObject(i+1) + ",";
                }
                res += sub;
            }
            }
            else if(s[0].equals("insert") || s[0].equals("update") || s[0].equals("delete")){
                Statement st = conn.createStatement();
                st.executeUpdate(rq);
                res = "ok";
            }
            else {
                Statement st = conn.createStatement();
                st.execute(rq);
                res = "ok";
            }
            return res;
        } catch (SQLException e) {
            crudLog.info(e.getMessage());
            throw new RuntimeException("Stop immediately");
        }
    }

    public String executeOperations(String op, String table, String[] valuesJSON) {
        String res  = "";
        if(op.toUpperCase().equals("SELECT")) {
            res = select(table);
        }
        if(op.toUpperCase().equals("INSERT")) {
            res = insert(table, valuesJSON);
        }
        if(op.toUpperCase().equals("DELETE")) {
          res =delete(table, valuesJSON);
        }
        if(op.toUpperCase().equals("UPDATE")) {
           res = update(table, valuesJSON);
        }
        return res;
    }

    public String select(String table) {
        try {
            String rq = "select * from " + table;
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(rq);
            String res = "";
            while (rs.next()) {
                String sub = "";
                int col_nb = rs.getMetaData().getColumnCount();
                for (int i = 0; i < col_nb; i++) {
                    sub += "" + rs.getMetaData().getColumnLabel(i+1) + " = " + rs.getObject(i+1) + " ";
                }
                sub += "\n";
                res += sub;
            }
            return res;
        } catch (SQLException e) {
            crudLog.info(e.getMessage());
            throw new RuntimeException("Stop immediately");
        }
    }
   public String insert(String table, String[] valuesJSON) {
    	try {
    		String rq=null;
    		Statement st = conn.createStatement();
    		Map<?,?> mapValues=null;
    		ObjectMapper mapper=new ObjectMapper();
    		
    		for(int i=0;i<valuesJSON.length;i++) {
    			//Delete the ',' to have a JSON element
    			if(valuesJSON[i].charAt(0)==',') {
    				valuesJSON[i]=valuesJSON[i].substring(1);
    			}
    		//Add '}' to complete the JSON element	
    	    mapValues=mapper.readValue(valuesJSON[i]+"}", Map.class);
    	    rq="INSERT INTO "+table+"(id,username,descrip) VALUES('"+mapValues.get("id")+"','"+mapValues.get("username")+"','"+mapValues.get("descrip")+"')";
    	    st.executeUpdate(rq);
    		}
    	    return "Insert request sucessfully executed!";
    	}catch(Exception e) {
    		crudLog.info(e.getMessage());
            throw new RuntimeException("Stop immediately");
    	}
    }
    public String delete(String table, String[] valuesJSON){
      	try {
    		String rq=null;
    		Statement st = conn.createStatement();
    		Map<?,?> mapValues=null;
    		ObjectMapper mapper=new ObjectMapper();
    		System.out.println(valuesJSON);
    		
    		for(int i=0;i<valuesJSON.length;i++) {
    			if(valuesJSON[i].charAt(0)==',') {
    				valuesJSON[i]=valuesJSON[i].substring(1);
    			}
    			
    	    mapValues=mapper.readValue(valuesJSON[i]+"}", Map.class);
            rq="DELETE FROM "+table+" where username='"+ mapValues.get("username")+ "' and descrip='"+ mapValues.get("descrip")+"'";
    	    st.executeUpdate(rq);
    		}
    	    return "Delete request sucessfully executed!";
    	}catch(Exception e) {
    		crudLog.info(e.getMessage());
            throw new RuntimeException("Stop immediately");
    	}
    }
 
    public String update(String table, String[] valuesJSON){
      	try {
    		String rq=null;
    		Statement st = conn.createStatement();
    		Map<?,?> mapValues=null;
    		ObjectMapper mapper=new ObjectMapper();
    		for(int i=0;i<valuesJSON.length;i++) {
    			if(valuesJSON[i].charAt(0)==',') {
    				valuesJSON[i]=valuesJSON[i].substring(1);
    			}
    	    mapValues=mapper.readValue(valuesJSON[i]+"}", Map.class);
            rq="UPDATE "+table+" SET "+ "username='"+ mapValues.get("username")+ "', descrip='"+ mapValues.get("descrip")+"' "+ "WHERE " + "id="+mapValues.get("id");
    	    st.executeUpdate(rq);
    		}
    	    return "Update request sucessfully executed!";
    	}catch(Exception e) {
    		crudLog.info(e.getMessage());
            throw new RuntimeException("Stop immediately");
    	}
    }
}

