package edu.episen.si.ing1.pds.metropolis.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ServerConfig {

    private final static Logger logger = LoggerFactory.getLogger(ServerConfig.class.getName());
    private static final String pdsServerConfigEnvVar = "PDS_SERVER_CONFIG";
    private final String serverConfigFileLocation;
    private ServerCoreConfig config;

    public ServerConfig() throws IOException {
        serverConfigFileLocation = System.getenv(pdsServerConfigEnvVar);
        logger.debug("Config file = {}", serverConfigFileLocation);

        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        config = mapper.readValue(new File(serverConfigFileLocation), ServerCoreConfig.class);
        logger.debug("Config = {}", config.toString());
    }

    public ServerCoreConfig getConfig() {
        return config;
    }
}
