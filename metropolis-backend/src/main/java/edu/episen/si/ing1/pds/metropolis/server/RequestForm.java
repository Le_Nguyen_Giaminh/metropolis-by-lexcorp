package edu.episen.si.ing1.pds.metropolis.server;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;

@JsonRootName(value = "request")
public class RequestForm {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public String requestId;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String requestBody;
	
	@JsonProperty("request_id")
	public void setRequestId(String requestId) {this.requestId=requestId;}
	
	@JsonSetter("request_body")
	public void setRequestContent(final String requestBody) {this.requestBody=requestBody;}
	
	public String getRequestId() {return requestId;}
	
	public String getRequestBody() {return requestBody;}
}

