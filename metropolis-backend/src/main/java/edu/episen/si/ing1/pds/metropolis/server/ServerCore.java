package edu.episen.si.ing1.pds.metropolis.server;

import connectionPool.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerCore {
    private static Logger serverLog = LoggerFactory.getLogger(ServerCore.class.getName());
    private ServerSocket ss;
    private DataSource ds;

    public ServerCore(DataSource ds, final ServerConfig config) throws IOException {
        ss = new ServerSocket(config.getConfig().getListenPort());
        ss.setSoTimeout(config.getConfig().getTimeOut());
        this.ds = ds;
    }

    public void serve() throws IOException, InterruptedException {
        serverLog.info("Metropolis server started");
        // wait for a client to arrive
       while(true) {
           Socket socket = ss.accept();
           final ClientRequestManager clientRequestManager = new ClientRequestManager(socket, ds);
       }
    }
}
