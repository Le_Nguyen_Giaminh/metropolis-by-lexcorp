package edu.episen.si.ing1.pds.metropolis.server;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import connectionPool.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.net.Socket;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class ClientRequestManager implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(ClientRequestManager.class.getName());
    private final InputStream inputStream;
    private final OutputStream outputStream;
    private static final String name = "client";
    private DataSource ds;
    private Thread self;
    private boolean isConnected;

    public ClientRequestManager(Socket socket, DataSource ds) throws IOException {
        super();
        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();
        this.ds = ds;
        isConnected = true;
        self = new Thread(this, name);
        self.start();
    }

    @Override
    public void run() {
        Connection conn = ds.getConnection();

        if (conn == null) {
            logger.info("No connection is available right now. Please try again later !");
            self.stop();
        } 
        else {
            while(isConnected) {
                byte[] inputData;
                try {
                    while(inputStream.available()<=0) {
                        self.sleep(1000);
                    }
                    inputData = new byte[inputStream.available()];
                    inputStream.read(inputData);
                    
                    logger.info("Data received : {} bytes, content = {}", inputData.length, new String(inputData));
                    final ObjectMapper mapper = new ObjectMapper(new JsonFactory());
			        RequestForm rqForm = mapper.readValue(inputData, RequestForm.class);

                    CRUDOperations crud = new CRUDOperations(conn);
                    String res = crud.executeRequest(rqForm.getRequestBody());

                    final Map<String, String> result = new HashMap<>();
                    result.put("requestId", rqForm.getRequestId());
                    result.put("responseBody", res);
                    DataOutputStream data = new DataOutputStream(outputStream);
                    data.write(mapper.writeValueAsBytes(result));
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    ds.addConnection(conn);
                }
            }
        }
    }

    public void join() throws InterruptedException {
        self.join();
    }
}
