package edu.episen.si.ing1.pds.metropolis.server;

import connectionPool.DataSource;
import connectionPool.JDBCConnectionPool;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class BackendService {

    private final static Logger logger = LoggerFactory.getLogger(BackendService.class.getName());
    public static ServerConfig serverConfig;

    public static void main(String[] args) throws ParseException, IOException, InterruptedException {

        final Options opts = new Options();
        final Option maxClientConnection = Option.builder().longOpt("maxClientConnection").hasArgs().argName("maxClientConnection").build();
        final Option maxPoolConnection = Option.builder().longOpt("maxPoolConnection").hasArgs().argName("maxPoolConnection").build();

        opts.addOption(maxClientConnection);
        opts.addOption(maxPoolConnection);

        final CommandLineParser parser = new DefaultParser();
        final CommandLine commandLine = parser.parse(opts,args);

        boolean inTestMode = false;
        int maxClientValue = 10;
        int maxPoolValue = 10;

        if (commandLine.hasOption("maxClientConnection")) {
            maxClientValue = Integer.parseInt(commandLine.getOptionValue("maxClientConnection"));
        }
        if (commandLine.hasOption("maxPoolConnection")) {
            maxPoolValue = Integer.parseInt(commandLine.getOptionValue("maxPoolConnection"));
        }

        serverConfig = new ServerConfig();
        JDBCConnectionPool p = new JDBCConnectionPool(maxPoolValue);
        p.createConnection();
        DataSource ds = new DataSource(p);
        ServerCore server = new ServerCore(ds, serverConfig);
        server.serve();
    }
}
