DROP TABLE IF EXISTS Building CASCADE; 

DROP TABLE IF EXISTS Room CASCADE; 

DROP TABLE IF EXISTS Company CASCADE; 

DROP TABLE IF EXISTS Floors CASCADE; 

DROP TABLE IF EXISTS reserve CASCADE; 

DROP TABLE IF EXISTS Type_of_Equipment CASCADE; 

DROP TABLE IF EXISTS choose CASCADE; 

DROP TABLE IF EXISTS Equipment CASCADE; 

DROP TABLE IF EXISTS Type_of_Spot CASCADE; 

DROP TABLE IF EXISTS Spot_for_equipment CASCADE; 

DROP TABLE IF EXISTS compatibility CASCADE; 

DROP TABLE IF EXISTS installed_equipment CASCADE; 

DROP TABLE IF EXISTS Employee CASCADE; 

DROP TABLE IF EXISTS Service CASCADE; 

DROP TABLE IF EXISTS Badge CASCADE; 

DROP TABLE IF EXISTS associate CASCADE; 

DROP TABLE IF EXISTS Configuration CASCADE; 

DROP TABLE IF EXISTS Windows CASCADE; 

DROP TABLE IF EXISTS Data_sensor CASCADE; 

 

  

CREATE TABLE Building( 

build_id serial, 

b_name VARCHAR(50), 

PRIMARY KEY(build_id) 

); 

  

CREATE TABLE Company( 

comp_id serial, 

c_name VARCHAR(50), 

PRIMARY KEY(comp_id) 

); 

 

CREATE TABLE Type_of_Equipment( 

equipment_type_id  INT PRIMARY KEY, 

name VARCHAR(50) NOT NULL 

); 

  

CREATE TABLE Equipment ( 

equipment_id serial PRIMARY KEY, 

equipment_type_id INT NOT NULL, 

description VARCHAR(100) 

); 



CREATE TABLE Floors( 

floor_id serial, 

f_name VARCHAR(50), 

build_id INT NOT NULL, 

PRIMARY KEY(floor_id), 

FOREIGN KEY(build_id) REFERENCES Building(build_id) 

); 

  

CREATE TABLE Room( 

   room_id serial, 

   r_name VARCHAR(50), 

   type VARCHAR(50), 

   size INT, 

   capacity INT, 

   available boolean, 

   floor_id INT NOT NULL, 

   PRIMARY KEY(room_id), 

   FOREIGN KEY(floor_id) REFERENCES Floors(floor_id) 

); 

CREATE TABLE Badge( 

badge_id INT, 

badge_name CHAR(50), 

PRIMARY KEY (badge_id) 

); 

CREATE TABLE Service( 

service_id INT, 

service_name CHAR(50), 

PRIMARY KEY (service_id) 

); 

CREATE TABLE Employee( 

employee_id INT, 

name CHAR(50), 

lastName CHAR(50), 

service INT, 

badgeId INT, 

PRIMARY KEY (employee_id), 

FOREIGN KEY (service) REFERENCES Service (service_id), 

FOREIGN KEY (badgeId) REFERENCES Badge(badge_id) 

); 

CREATE TABLE associate( 

associate_id INT, 

roomId INT, 

employee_id INT, 

PRIMARY KEY (associate_id), 

FOREIGN KEY (roomId) REFERENCES Room (room_id), 

FOREIGN KEY (employee_id) REFERENCES Employee (employee_id) 

); 

CREATE TABLE reserve(
   room_id INT,
   comp_id INT,
   valid boolean,
   date_reserv DATE,
   PRIMARY KEY(room_id, comp_id),
   FOREIGN KEY(room_id) REFERENCES Room(room_id),
   FOREIGN KEY(comp_id) REFERENCES Company(comp_id)
);



CREATE TABLE Type_of_Spot( 

spot_type_id INT PRIMARY KEY, 

name VARCHAR(50) 

); 

 

CREATE TABLE Spot_for_equipment ( 

spot_id serial PRIMARY KEY, 

spot_type_id INT, 

room_id INT, 

position_x INT NOT NULL, 

position_y INT NOT NULL, 

FOREIGN KEY(room_id) REFERENCES Room(room_id), 

FOREIGN KEY(spot_type_id) REFERENCES Type_of_Spot(spot_type_id) 

); 

 

CREATE TABLE compatibility( 

spot_type_id INT, 

equipment_type_id INT, 

PRIMARY KEY(spot_type_id, equipment_type_id), 

FOREIGN KEY(spot_type_id) REFERENCES Type_of_Spot(spot_type_id), 

FOREIGN KEY(equipment_type_id) REFERENCES Type_of_Equipment(equipment_type_id) 

); 

  

CREATE TABLE installed_equipment( 

spot_id INT NOT NULL, 

equipment_id INT NOT NULL, 

state BOOLEAN NOT NULL, 

PRIMARY KEY(spot_id, equipment_id), 

FOREIGN KEY(spot_id) REFERENCES spot_for_equipment(spot_id), 

FOREIGN KEY(equipment_id) REFERENCES equipment(equipment_id) 

); 

create table Windows( 

id serial primary key not null, 

is_reserved boolean not null, 

position_x INT NOT null,  

position_y INT NOT null,  

window_state varchar(30) not null, 

percentage_blind int not null, 

percentage_opacity int not null, 

room_temperature int not null, 

room_id int, 

foreign key(room_id) references Room(room_id) 

); 

create table Data_sensor( 

id serial primary key not null, 

level_sunlight int not null, 

outside_temperature int not null, 

window_id int, 

foreign key(window_id) references Windows(id) 

); 

create table Configuration( 

id serial primary key not null, 

window_state varchar(30), 

blind_choice varchar(30) not null, 

one_percentage int, 

blind_level_start int, 

blind_percentage_start int, 

blind_level_add int, 

blind_percentage_add int, 

opacity_choice varchar(30) not null, 

one_opacity int, 

opacity_level_start int, 

opacity_percentage_start int, 

opacity_level_add int, 

opacity_percentage_add int, 

temperature_choice varchar(30) not null, 

ideal_temperature int,

window_id int, 

foreign key(window_id) references Windows(id) 

); 

 

  

INSERT INTO building (build_id, b_name) 
VALUES (1, 'Batiment A'), (2, 'Batiment B'), (3, 'Batiment C'), (4, 'Batiment D'); 

 
INSERT INTO floors (floor_id, f_name, build_id) 
VALUES (1, 'Etage 1', 1), (2, 'Etage 2', 1), (3, 'Etage 3', 1), (4, 'Etage 1', 2), (5, 'Etage 2', 2), (6, 'Etage 3', 2), (7, 'Etage 1', 3), (8, 'Etage 2', 3), (9, 'Etage 3', 3), (10, 'Etage 1', 4), (11, 'Etage 2', 4), (12, 'Etage 3', 4); 

 
INSERT INTO company (comp_id, c_name) 
VALUES (1, 'Tesla'), (2, 'SBS'), (3, 'Amazon'), (4, 'Wayne Enterprises'); 

 

--insert dans la table room 
do $$   

declare  cpt integer := 1;     

begin   

while cpt<=12 loop   

INSERT INTO room(r_name,type,size, capacity, available, floor_id) VALUES('PSDR','PSDR',25,8,true,cpt),('GSDR','GSDR',35,12,true,cpt),('OS','OS',50,20,true,cpt);    

cpt:= cpt + 1;    

end loop;   

end $$; 

 

do $$   

declare cpt integer := 1;   

begin   

while cpt<=12 loop   

for i in 1..20 loop   

INSERT INTO room(r_name,type,size, capacity, available, floor_id) VALUES(CONCAT('EI',i),'EI',4,1,true,cpt);   

end loop;   

cpt:=cpt+1;   

end loop;    

end $$; 

--fin insert dans la table room 


INSERT INTO Type_of_Equipment(equipment_type_id,name) VALUES (1,'Ordinateur'), (2,'Ecran'), (3,'HUB USB'), (4,'Detecteur de fumee'), (5,'Detecteur de présence'), (6,'Chargeur sans fil'), (7,'Ecran de projection'), (8,'Projecteur'), (9,'Tablette graphique'), (10,'Imprimante'); 

INSERT INTO Type_of_Spot(spot_type_id,name) VALUES (1,'Table'), (2,'Ecran'),(3,'Detecteur'),(4,'Projecteur'),(5,'Imprimante'),(6,'Ecran de projecteur'); 

INSERT INTO compatibility(equipment_type_id, spot_type_id) VALUES (1,1),(2,2),(3,1),(4,3),(5,3),(6,1),(7,6),(8,4),(9,1),(10,5); 

--insert dans la table equipment
do $$
declare cpt type_of_equipment%rowtype;
declare i integer;
begin
for cpt in select * from type_of_equipment loop
for i in 1..20 loop
INSERT INTO equipment(equipment_type_id, description) VALUES (cpt.equipment_type_id, cpt.name);
end loop;
end loop;
end $$;
--fin insert dans la table equipment


--insert dans la table Spot_for_Equipment 

do $$ 

declare cpt integer; 

begin 

for cpt in select room.room_id from room where room.type='PSDR' 

loop 

INSERT INTO Spot_for_Equipment(spot_type_id,room_id,position_x,position_y) VALUES (5,cpt,0,0),(2,cpt,3,0),(1,cpt,1,1),(4,cpt,2,1),(6,cpt,3,1),(3,cpt,3,2),(1,cpt,1,3),(1,cpt,2,3),(3,cpt,3,3); 

end loop; 

end $$; 
  

do $$ 

declare cpt integer; 

begin 

for cpt in select room.room_id from room where room.type='GSDR' 

loop 

INSERT INTO Spot_for_Equipment(spot_type_id,room_id,position_x,position_y) VALUES (5,cpt,1,0),(1,cpt,2,0),(3,cpt,3,0),(1,cpt,1,2),(1,cpt,2,2),(2,cpt,3,2),(1,cpt,1,3),(4,cpt,2,3),(6,cpt,3,3),(3,cpt,0,4),(1,cpt,1,5),(1,cpt,2,5); 

end loop; 

end $$; 


do $$ 

declare cpt integer; 

begin 

for cpt in select room.room_id from room where room.type='OS' 

loop 

INSERT INTO Spot_for_Equipment(spot_type_id,room_id,position_x,position_y) VALUES (3,cpt,4,0),(5,cpt,5,0),(1,cpt,1,1),(1,cpt,2,1),(1,cpt,3,1),(1,cpt,4,1),(1,cpt,1,2),(1,cpt,2,2),(4,cpt,3,2),(2,cpt,4,2),(3,cpt,0,3),(6,cpt,3,3); 

end loop; 

end $$; 
  

do $$ 

declare cpt integer; 

begin 

for cpt in select room.room_id from room where room.type='EI' 

loop 

INSERT INTO Spot_for_Equipment(spot_type_id,room_id,position_x,position_y) VALUES (1,cpt,0,0),(1,cpt,0,1),(1,cpt,1,0); 

end loop; 

end $$; 

--fin de insert dans la table Spot_for_Equipment 

 

--insert dans la table windows : 

--insert les fenetre des OS 

do $$ 

declare cpt integer; 

begin 

for cpt in select room.room_id from room where room.type='OS' 

loop 

insert into windows(is_reserved,position_x,position_y,window_state,percentage_blind,percentage_opacity,room_temperature,room_id) values (true,0,0,'fermé',0,0,20,cpt); 

insert into windows(is_reserved,position_x,position_y,window_state,percentage_blind,percentage_opacity,room_temperature,room_id) values (true,0,1,'fermé',0,0,20,cpt); 

insert into windows(is_reserved,position_x,position_y,window_state,percentage_blind,percentage_opacity,room_temperature,room_id) values (true,0,2,'fermé',0,0,20,cpt); 

insert into windows(is_reserved,position_x,position_y,window_state,percentage_blind,percentage_opacity,room_temperature,room_id) values (true,0,3,'fermé',0,0,20,cpt); 

end loop; 

end $$; 

--insert les fenetre des GSDR 

do $$ 

declare cpt integer; 

begin 

for cpt in select room.room_id from room where room.type='GSDR' 

loop 

insert into windows(is_reserved,position_x,position_y,window_state,percentage_blind,percentage_opacity,room_temperature,room_id) values (true,3,1,'fermé',0,0,20,cpt); 

insert into windows(is_reserved,position_x,position_y,window_state,percentage_blind,percentage_opacity,room_temperature,room_id) values (true,3,4,'fermé',0,0,20,cpt); 

end loop; 

end $$; 

--insert les fenetre des PSDR 

do $$ 

declare cpt integer; 

begin 

for cpt in select room.room_id from room where room.type='PSDR' 

loop 

insert into windows(is_reserved,position_x,position_y,window_state,percentage_blind,percentage_opacity,room_temperature,room_id) values (true,3,2,'fermé',0,0,20,cpt); 

end loop; 

end $$; 

--fin de insert dans la table windows 

--insert dans la table configuration

do $$

declare cpt integer;

begin

for cpt in select windows.id from windows 

loop

insert into configuration(window_state,blind_choice,one_percentage,blind_level_start,blind_percentage_start,blind_level_add,blind_percentage_add,opacity_choice,one_opacity,opacity_level_start,opacity_percentage_start,opacity_level_add,opacity_percentage_add,temperature_choice,ideal_temperature,window_id) values ('fermé','choix1',0,null,null,null,null,'choix1',0,null,null,null,null,'choix1',null,cpt);

end loop;

end $$;

--fin de insert dans la table configuration

--insert dans la table data_sensor

do $$

declare cpt integer;

begin

for cpt in select windows.id from windows

loop

insert into data_sensor(level_sunlight,outside_temperature,window_id) values (200,20,cpt);

end loop;

end $$;

--fin de insert dans la table data_sensor
