package rent.digitalworkplace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

public class RequestResults {
	private ClientCore client;

	public RequestResults(ClientCore client) {
		this.client = client;
	}

	public HashMap<String, List<String>> getFreeRooms() {
		String rq = "select b.b_name, r.room_id, r.type, r.size, r.capacity, f.f_name " + "from room r "
				+ "inner join floors f " + "on r.floor_id = f.floor_id " + "inner join building b "
				+ "on f.build_id = b.build_id where r.available = true";
		String rp = client.sendRequest(rq).getResponseBody();
		HashMap<String, List<String>> res = new HashMap<>();

		String[] s1 = rp.split(",");
		List<String> list_room_id = new ArrayList<>();
		List<String> list_name_b = new ArrayList<>();
		List<String> list_size = new ArrayList<>();
		List<String> list_capacity = new ArrayList<>();
		List<String> list_floor_num = new ArrayList<>();
		List<String> list_type = new ArrayList<>();

		for (int i = 0; i < s1.length; i++) {
			String[] s2 = s1[i].split("=");
			if (s2[0].equals("size")) {
				list_size.add(s2[1]);
			} else if (s2[0].equals("capacity")) {
				list_capacity.add(s2[1]);
			} else if (s2[0].equals("room_id")) {
				list_room_id.add(s2[1]);
			} else if (s2[0].equals("b_name")) {
				list_name_b.add(s2[1]);
			} else if (s2[0].equals("type")) {
				list_type.add(s2[1]);
			} else if (s2[0].equals("f_name")) {
				list_floor_num.add(s2[1]);
			}
		}

		res.put("Room_id", list_room_id);
		res.put("Building", list_name_b);
		res.put("Type", list_type);
		res.put("Size", list_size);
		res.put("Capacity", list_capacity);
		res.put("Floor", list_floor_num);
		return res;

	}

	public HashMap<String, List<String>> getFreeReunion() {
		String rq = "select b.b_name, r.room_id, r.type, r.size, r.capacity, f.f_name " + "from room r "
				+ "inner join floors f " + "on r.floor_id = f.floor_id " + "inner join building b "
				+ "on f.build_id = b.build_id " + "where r.type = 'PSDR' or r.type ='GSDR' and r.available = true";
		String rp = client.sendRequest(rq).getResponseBody();
		HashMap<String, List<String>> res = new HashMap<>();

		String[] s1 = rp.split(",");
		List<String> list_room_id = new ArrayList<>();
		List<String> list_name_b = new ArrayList<>();
		List<String> list_size = new ArrayList<>();
		List<String> list_capacity = new ArrayList<>();
		List<String> list_floor_num = new ArrayList<>();
		List<String> list_type = new ArrayList<>();

		for (int i = 0; i < s1.length; i++) {
			String[] s2 = s1[i].split("=");
			if (s2[0].equals("size")) {
				list_size.add(s2[1]);
			} else if (s2[0].equals("capacity")) {
				list_capacity.add(s2[1]);
			} else if (s2[0].equals("room_id")) {
				list_room_id.add(s2[1]);
			} else if (s2[0].equals("b_name")) {
				list_name_b.add(s2[1]);
			} else if (s2[0].equals("type")) {
				list_type.add(s2[1]);
			} else if (s2[0].equals("f_name")) {
				list_floor_num.add(s2[1]);
			}
		}

		res.put("Room_id", list_room_id);
		res.put("Building", list_name_b);
		res.put("Type", list_type);
		res.put("Size", list_size);
		res.put("Capacity", list_capacity);
		res.put("Floor", list_floor_num);
		return res;

	}

	public HashMap<String, List<String>> getFreeSolo() {
		String rq = "select b.b_name, r.room_id, r.type, r.size, r.capacity, f.f_name " + "from room r "
				+ "inner join floors f " + "on r.floor_id = f.floor_id " + "inner join building b "
				+ "on f.build_id = b.build_id " + "where r.type = 'EI' and r.available = true";
		String rp = client.sendRequest(rq).getResponseBody();
		HashMap<String, List<String>> res = new HashMap<>();

		String[] s1 = rp.split(",");
		List<String> list_room_id = new ArrayList<>();
		List<String> list_name_b = new ArrayList<>();
		List<String> list_size = new ArrayList<>();
		List<String> list_capacity = new ArrayList<>();
		List<String> list_floor_num = new ArrayList<>();
		List<String> list_type = new ArrayList<>();

		for (int i = 0; i < s1.length; i++) {
			String[] s2 = s1[i].split("=");
			if (s2[0].equals("size")) {
				list_size.add(s2[1]);
			} else if (s2[0].equals("capacity")) {
				list_capacity.add(s2[1]);
			} else if (s2[0].equals("room_id")) {
				list_room_id.add(s2[1]);
			} else if (s2[0].equals("b_name")) {
				list_name_b.add(s2[1]);
			} else if (s2[0].equals("type")) {
				list_type.add(s2[1]);
			} else if (s2[0].equals("f_name")) {
				list_floor_num.add(s2[1]);
			}
		}

		res.put("Room_id", list_room_id);
		res.put("Building", list_name_b);
		res.put("Type", list_type);
		res.put("Size", list_size);
		res.put("Capacity", list_capacity);
		res.put("Floor", list_floor_num);
		return res;

	}

	public HashMap<String, List<String>> getFreeOS() {
		String rq = "select b.b_name, r.room_id, r.type, r.size, r.capacity, f.f_name " +
		// ", c.room_id" from choose c inner join room r "
				"from room r " +
				// "on r.room_id = c.room_id " +
				"inner join floors f " + "on r.floor_id = f.floor_id " + "inner join building b "
				+ "on f.build_id = b.build_id " + "where r.type = 'OS' and r.available = true";
		String rp = client.sendRequest(rq).getResponseBody();
		HashMap<String, List<String>> res = new HashMap<>();

		String[] s1 = rp.split(",");
		List<String> list_room_id = new ArrayList<>();
		List<String> list_name_b = new ArrayList<>();
		List<String> list_size = new ArrayList<>();
		List<String> list_capacity = new ArrayList<>();
		List<String> list_floor_num = new ArrayList<>();
		List<String> list_type = new ArrayList<>();

		for (int i = 0; i < s1.length; i++) {
			String[] s2 = s1[i].split("=");
			if (s2[0].equals("size")) {
				list_size.add(s2[1]);
			} else if (s2[0].equals("capacity")) {
				list_capacity.add(s2[1]);
			} else if (s2[0].equals("room_id")) {
				list_room_id.add(s2[1]);
			} else if (s2[0].equals("b_name")) {
				list_name_b.add(s2[1]);
			} else if (s2[0].equals("type")) {
				list_type.add(s2[1]);
			} else if (s2[0].equals("f_name")) {
				list_floor_num.add(s2[1]);
			}
		}

		res.put("Room_id", list_room_id);
		res.put("Building", list_name_b);
		res.put("Type", list_type);
		res.put("Size", list_size);
		res.put("Capacity", list_capacity);
		res.put("Floor", list_floor_num);
		return res;

	}

	public static String ifAvailable(ClientCore client, String str) {
		String state = "";
		String rqSel = "select available from Room where room_id = " + str;
		String rp = client.sendRequest(rqSel).getResponseBody();
		String[] s1 = rp.split(",");
		for (int i = 0; i < s1.length; i++) {
			String[] s2 = s1[i].split("=");
			if (s2[0].equals("available")) {
				state = s2[1];
			}

		}
		return state;
	}

}
