package rent.digitalworkplace;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;
import main_page.Menu;

public class IHMFloorMap extends JFrame implements ActionListener, ItemListener {
	private ClientCore client;
	private String name;
	private String id;
	private String numebat;
	private JPanel generalPanel = new JPanel(new BorderLayout());
	private JPanel panel1 = new JPanel();
	private JLabel building = new JLabel("Batiment :");
	private String[] numbat = { "A", "B", "C", "D" };
	private JComboBox buildingChoice = new JComboBox(numbat);
	private JLabel floor = new JLabel("Etage :");
	private String[] numet = { "1", "2", "3" };
	private JComboBox floorChoice = new JComboBox(numet);
	private JPanel panel2;
	private JPanel panel3 = new JPanel();
	private JButton returnButton = new JButton("Retour");
	private JButton menu = new JButton("Menu");
	private String date = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

	public IHMFloorMap(ClientCore client, String name, String id, String numebat) {
		this.client = client;
		this.name = name;
		this.id = id;
		this.numebat = numebat;

		// first panel
		panel1.setLayout(new GridLayout(1, 3));
		JPanel panel11 = new JPanel(new FlowLayout());
		buildingChoice.setSelectedItem(numebat);
		panel11.add(building);
		panel11.add(buildingChoice);
		JPanel panel12 = new JPanel(new FlowLayout());
		panel12.add(floor);
		panel12.add(floorChoice);
		panel1.add(panel11);
		panel1.add(panel12);
		generalPanel.add(panel1, BorderLayout.NORTH);

		// second panel
		panel2 = planFloor();
		generalPanel.add(panel2, BorderLayout.CENTER);

		// third panel
		JLabel lb1 = new JLabel("EI : Espace individuel / ");
		JLabel lb2 = new JLabel("PSDR : Petite salle de réunion / ");
		JLabel lb3 = new JLabel("GSDR : Grande salle de réunion / ");
		JLabel lb4 = new JLabel("OS : Open space");
		panel3.add(lb1);
		panel3.add(lb2);
		panel3.add(lb3);
		panel3.add(lb4);
		returnButton.addActionListener(this);
		panel3.add(returnButton);
		menu.addActionListener(this);
		panel3.add(menu);
		generalPanel.add(panel3, BorderLayout.SOUTH);
		generalPanel.setBorder(new TitledBorder("Plan des etages"));
		this.add(generalPanel);
	}

	public JPanel planFloor() {
		JPanel p = new JPanel(new GridLayout(7, 15));
		JPanel[][] colorRoom = new JPanel[7][15];

		Border lineBorder = BorderFactory.createLineBorder(Color.BLACK, 1);
		p.setBorder(lineBorder);
		int numei = 1;
		for (int i = 0; i <= 1; i++) { // individual spaces
			for (int j = 0; j <= 9; j++) {
				colorRoom[i][j] = new JPanel();
				colorRoom[i][j].setBackground(new Color(180, 128, 177));
				colorRoom[i][j].setBorder(lineBorder);
				JButton EI = new JButton("EI" + numei);
				numei++;
				colorRoom[i][j].add(EI);
				EI.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						String roomID = "";
						String rqSel = "select r.room_id from Room as r "
								+ "left join Floors as f on r.floor_id = f.floor_id left join Building as b"
								+ " on f.build_id = b.build_id where r.r_name = '"
								+ EI.getText() + "' and b.b_name = 'Batiment " + buildingChoice.getSelectedItem()
								+ "' and f.f_name = 'Etage " + floorChoice.getSelectedItem() + "'";
						String rp = client.sendRequest(rqSel).getResponseBody();
						String[] s1 = rp.split(",");
						for (int i = 0; i < s1.length; i++) {
							String[] s2 = s1[i].split("=");
							if (s2[0].equals("room_id")) {
								roomID = s2[1];
							}
						}

						if (RequestResults.ifAvailable(client, roomID).equals("true")) {
							Object[] options = { "Cest sûr !", "Finalement non" };

							int response = JOptionPane.showOptionDialog(null,
									"Etes-vous sûr de vouloir réserver cet espace ?", "Confirmation de réservation",
									JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

							if (response == JOptionPane.YES_OPTION) {

								// Register the reservation
								String rqIns = "insert into reserve(room_id, comp_id, valid, date_reserv) values ("
										+ roomID + ", " + "(select comp_id from Company where c_name = '" + name
										+ "'), true, '" + date + "')";
								client.sendRequest(rqIns);

								// Make the room unavailable
								String rqUp = "update Room set available = false where room_id = " + roomID;
								client.sendRequest(rqUp);
								JOptionPane.showMessageDialog(null, "<html>La réservation de la salle " + roomID
										+ " a bien été enregistrée au " + date + " sur le compte de l'entreprise "
										+ name
										+ ". <br><center>Vous pouvez y placer jusqu'à 3 équipements via l'onglet mapping.</center>", "Réservation réussie", JOptionPane.INFORMATION_MESSAGE);
							} else {
								System.out.println("Refus de réservation.");
							}
						}

						else {
							JOptionPane.showMessageDialog(null, "Cet espace est déjà reservé.", "Indisponibilité",
									JOptionPane.ERROR_MESSAGE);
						}

					}
				});
			}
		}

		for (int i = 3; i <= 6; i++) { // small meeting room
			for (int j = 0; j <= 3; j++) {
				colorRoom[i][j] = new JPanel();
				colorRoom[i][j].setBackground(new Color(122, 206, 93));
			}
		}
		JButton PSDR = new JButton("PSDR");
		PSDR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String roomID = "";
				String rqSel = "select r.room_id from Room as r " + "left join Floors as f on r.floor_id = f.floor_id "
						+ "left join Building as b on f.build_id = b.build_id "
						+ "where r.r_name = 'PSDR' and b.b_name = 'Batiment " + buildingChoice.getSelectedItem()
						+ "' and f.f_name = 'Etage " + floorChoice.getSelectedItem() + "'";
				String rp = client.sendRequest(rqSel).getResponseBody();
				String[] s1 = rp.split(",");
				for (int i = 0; i < s1.length; i++) {
					String[] s2 = s1[i].split("=");
					if (s2[0].equals("room_id")) {
						roomID = s2[1];
					}
				}

				if (RequestResults.ifAvailable(client, roomID).equals("true")) {
					Object[] options = { "Cest sûr !", "Finalement non" };

					int response = JOptionPane.showOptionDialog(null, "Etes-vous sûr de vouloir réserver cet espace ?",
							"Confirmation de réservation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
							null, options, options[0]);

					if (response == JOptionPane.YES_OPTION) {

						// Register the reservation
						String rqIns = "insert into reserve(room_id, comp_id, valid, date_reserv) values (" + roomID
								+ ", " + "(select comp_id from Company where c_name = '" + name + "'), true, '" + date
								+ "')";
						client.sendRequest(rqIns);

						// Make the room unavailable
						String rqUp = "update Room set available = false where room_id = " + roomID;
						client.sendRequest(rqUp);
						JOptionPane.showMessageDialog(null, "<html>La réservation de la salle " + roomID
								+ " a bien été enregistrée au " + date + " sur le compte de l'entreprise " + name
								+ ". <br><center>Vous pouvez y placer jusqu'à 9 équipements via l'onglet mapping.</center>", "Réservation réussie", JOptionPane.INFORMATION_MESSAGE);
					} else {
						System.out.println("Refus de réservation.");
					}
				}

				else {
					JOptionPane.showMessageDialog(null, "Cet espace est déjà reservé.", "Indisponibilité",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		colorRoom[4][1].add(PSDR);

		for (int i = 3; i <= 6; i++) { // large meeting room
			for (int j = 4; j <= 9; j++) {
				colorRoom[i][j] = new JPanel();
				colorRoom[i][j].setBackground(new Color(237, 229, 113));
			}
		}
		JButton GSDR = new JButton("GSDR");
		GSDR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String roomID = "";
				String rqSel = "select r.room_id from Room as r " + "left join Floors as f on r.floor_id = f.floor_id "
						+ "left join Building as b on f.build_id = b.build_id "
						+ "where r.r_name = 'GSDR' and b.b_name = 'Batiment " + buildingChoice.getSelectedItem()
						+ "' and f.f_name = 'Etage " + floorChoice.getSelectedItem() + "'";
				String rp = client.sendRequest(rqSel).getResponseBody();
				String[] s1 = rp.split(",");
				for (int i = 0; i < s1.length; i++) {
					String[] s2 = s1[i].split("=");
					if (s2[0].equals("room_id")) {
						roomID = s2[1];
					}
				}

				if (RequestResults.ifAvailable(client, roomID).equals("true")) {

					Object[] options = { "Cest sûr !", "Finalement non" };

					int response = JOptionPane.showOptionDialog(null, "Etes-vous sûr de vouloir réserver cet espace ?",
							"Confirmation de réservation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
							null, options, options[0]);

					if (response == JOptionPane.YES_OPTION) {

						// Register the reservation
						String rqIns = "insert into reserve(room_id, comp_id, valid, date_reserv) values (" + roomID
								+ ", " + "(select comp_id from Company where c_name = '" + name + "'), true, '" + date
								+ "')";
						client.sendRequest(rqIns);

						// Make the room unavailable
						String rqUp = "update Room set available = false where room_id = " + roomID;
						client.sendRequest(rqUp);
						JOptionPane.showMessageDialog(null, "<html>La réservation de la salle " + roomID
								+ " a bien été enregistrée au " + date + " sur le compte de l'entreprise " + name
								+ ". <br><center>Vous pouvez y placer jusqu'à 12 équipements via l'onglet mapping.</center>", "Réservation réussie", JOptionPane.INFORMATION_MESSAGE);
					} else {
						System.out.println("Refus de réservation.");
					}
				}

				else {
					JOptionPane.showMessageDialog(null, "Cet espace est déjà reservé.", "Indisponibilité",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		colorRoom[4][6].add(GSDR);

		for (int i = 0; i <= 5; i++) { // open space
			for (int j = 11; j <= 14; j++) {
				colorRoom[i][j] = new JPanel();
				colorRoom[i][j].setBackground(new Color(46, 127, 245));
			}
		}
		JButton os = new JButton("OS");
		os.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String roomID = "";
				String rqSel = "select r.room_id from Room as r " + "left join Floors as f on r.floor_id = f.floor_id "
						+ "left join Building as b on f.build_id = b.build_id "
						+ "where r.r_name = 'OS' and b.b_name = 'Batiment " + buildingChoice.getSelectedItem()
						+ "' and f.f_name = 'Etage " + floorChoice.getSelectedItem() + "'";
				String rp = client.sendRequest(rqSel).getResponseBody();
				String[] s1 = rp.split(",");
				for (int i = 0; i < s1.length; i++) {
					String[] s2 = s1[i].split("=");
					if (s2[0].equals("room_id")) {
						roomID = s2[1];
					}

				}

				if (RequestResults.ifAvailable(client, roomID).equals("true")) {

					Object[] options = { "Cest sûr !", "Finalement non" };

					int response = JOptionPane.showOptionDialog(null, "Etes-vous sûr de vouloir réserver cet espace ?",
							"Confirmation de réservation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
							null, options, options[0]);

					if (response == JOptionPane.YES_OPTION) {

						// Register the reservation
						String rqIns = "insert into reserve(room_id, comp_id, valid, date_reserv) values (" + roomID
								+ ", " + "(select comp_id from Company where c_name = '" + name + "'), true, '" + date
								+ "')";
						client.sendRequest(rqIns);

						// Make the room unavailable
						String rqUp = "update Room set available = false where room_id = " + roomID;
						client.sendRequest(rqUp);
						JOptionPane.showMessageDialog(null, "<html>La réservation de la salle " + roomID
								+ " a bien été enregistrée au " + date + " sur le compte de l'entreprise " + name
								+ ". <br><center>Vous pouvez y placer jusqu'à 12 équipements via l'onglet mapping.</center>", "Réservation réussie", JOptionPane.INFORMATION_MESSAGE);
					} else {
						System.out.println("Refus de réservation.");
					}
				}

				else {
					JOptionPane.showMessageDialog(null, "Cet espace est déjà reservé.", "Indisponibilité",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		colorRoom[3][12].add(os);

		for (int j = 0; j <= 9; j++) { // corridor1
			colorRoom[2][j] = new JPanel();
			colorRoom[2][j].setBackground(Color.LIGHT_GRAY);
		}
		for (int i = 0; i <= 5; i++) { // corridor2
			colorRoom[i][10] = new JPanel();
			colorRoom[i][10].setBackground(Color.LIGHT_GRAY);
		}
		for (int j = 10; j <= 14; j++) { // corridor3
			colorRoom[6][j] = new JPanel();
			colorRoom[6][j].setBackground(Color.LIGHT_GRAY);
		}
		for (int i = 0; i <= 6; i++) { // add color to p
			for (int j = 0; j <= 14; j++) {
				p.add(colorRoom[i][j]);
			}
		}
		return p;
	}

	public void showWindow() {
		this.setTitle("Reserver une salle via la carte");
		this.pack();
		this.setSize(900, 700);
		this.setLocationRelativeTo(null);
		this.setTitle("Choisissez vos espaces à louer via la carte des étages.");
		this.setVisible(true);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == returnButton) {
			this.dispose();
		} else if (e.getSource() == menu) {
			Menu m = new Menu(client, name, id);
			dispose();
			m.setVisible(true);
			m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}

	}
}
