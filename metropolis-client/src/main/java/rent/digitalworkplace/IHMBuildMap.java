package rent.digitalworkplace;

import javax.swing.*;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IHMBuildMap extends JFrame implements ActionListener {
	private ClientCore client;
	private String name;
	private String id;
	private static final String pdsIconsEnvVar = "PDS_FRAMES_ICONS";
	private final static String iconsFileLocation = System.getenv(pdsIconsEnvVar);
	public Icon icon = new ImageIcon(iconsFileLocation + "/buildin.png");
	public JButton btn = new JButton(icon);
	public JButton btn2 = new JButton(icon);
	public JButton btn3 = new JButton(icon);
	public JButton btn4 = new JButton(icon);

	private JFrame fenetre = new JFrame();

	// Putting our map background
	class MonPanneau extends JPanel {
		public void paintComponent(Graphics g) {
			Image image = new ImageIcon(iconsFileLocation + "/fond.jpg").getImage();
			g.drawImage(image, 0, 0, this);
			g.setFont(new Font("default", Font.BOLD, 20));
			g.drawString("A", 113, 170);
			g.drawString("B", 290, 185);
			g.drawString("C", 210, 440);
			g.drawString("D", 373, 462);
		}
	}

	IHMBuildMap(ClientCore client, String name, String id) {
		this.client = client;
		this.name = name;
		this.id = id;

		// Localisize the buttons representing our 4 buildings
		MonPanneau pannel = new MonPanneau();
		btn.setBounds(93, 30, 55, 120);
		btn.addActionListener(this);
		fenetre.add(btn);
		btn2.setBounds(270, 45, 55, 120);
		btn2.addActionListener(this);
		JLabel bat2 = new JLabel("2");
		
		fenetre.add(btn2);
		btn3.setBounds(190, 300, 55, 120);
		btn3.addActionListener(this);
		fenetre.add(btn3);
		btn4.setBounds(350, 320, 55, 120);
		btn4.addActionListener(this);
		fenetre.add(btn4);

		// Configuration of the frame
		fenetre.setTitle("Choisissez votre emplacement.");
		fenetre.setSize(505, 530);
		fenetre.setLocationRelativeTo(null);
		fenetre.getContentPane().add(pannel);
		fenetre.setVisible(true);

	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btn) {
			String numebat = "A";
			IHMFloorMap carteetage = new IHMFloorMap(client, name, id, numebat);
			carteetage.showWindow();
			this.setVisible(false);
			this.dispose();
		} else if (e.getSource() == btn2) {
			String numebat = "B";
			IHMFloorMap carteetage = new IHMFloorMap(client, name, id, numebat);
			carteetage.showWindow();
			this.setVisible(false);
			this.dispose();
		} else if (e.getSource() == btn3) {
			String numebat = "C";
			IHMFloorMap carteetage = new IHMFloorMap(client, name, id, numebat);
			carteetage.showWindow();
			this.setVisible(false);
			this.dispose();
		} else if (e.getSource() == btn4) {
			String numebat = "D";
			IHMFloorMap carteetage = new IHMFloorMap(client, name, id, numebat);
			carteetage.showWindow();
			this.setVisible(false);
			this.dispose();
		}
	}

}