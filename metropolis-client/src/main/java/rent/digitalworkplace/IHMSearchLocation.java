package rent.digitalworkplace;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;
import main_page.Menu;

@SuppressWarnings("serial")
public class IHMSearchLocation extends JFrame implements ActionListener {
	private ClientCore client;
	private String name;
	private String id;
	private RequestResults resultat;
	private String[] typeSalle = { "Peu importe", "Salle de reunion fermée", "Espace individualisé", "Open Space" };
	private JComboBox listSalle = new JComboBox(typeSalle);
	private int nb = 1;
	private int nbm = 50;
	private double calcul = 0;
	private JTextField saisienbsalle = new JTextField(5);
	private JTextField saisieminpers = new JTextField(5);
	private JTextField saisiemaxpers = new JTextField(5);
	private JTextField saisietaille = new JTextField(5);
	private JPanel cp = new JPanel();
	private JPanel resu = new JPanel();
	private JPanel panel = new JPanel(new GridLayout(6, 2, 10, 10));
	private JPanel barre = new JPanel();
	private JLabel budget = new JLabel("Le budget estimé est de " + calcul + " €");
	private JButton menu = new JButton("Menu");
	private JButton button = new JButton();
	private String date = LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

	public IHMSearchLocation(RequestResults res, ClientCore client, String name, String id) {
		this.name = name;
		this.client = client;
		resultat = res;
		this.id = id;

		// Defining the frame
		setTitle("Recherche de salle(s).");
		setSize(676, 730);
		setLocationRelativeTo(null);

		// Putting a caption for definitions of types
		add(barre, BorderLayout.SOUTH);
		JLabel lb1 = new JLabel("EI : Espace individuel / ");
		JLabel lb2 = new JLabel("PSDR : Petite salle de réunion / ");
		JLabel lb3 = new JLabel("GSDR : Grande salle de réunion / ");
		JLabel lb4 = new JLabel("OS : Open space");

		//Add the menu button
		menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Menu m = new Menu(client, name, id);
				m.setVisible(true);
				dispose();
				m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
		
		barre.add(menu);
		barre.add(lb1);
		barre.add(lb2);
		barre.add(lb3);
		barre.add(lb4);

		// Putting the search feature and the possibility to reserve through a map
		// Foreground panel
		add(cp, BorderLayout.NORTH);
		cp.setPreferredSize(new Dimension(400, 230));
		cp.add(panel);

		// Room type
		JLabel typesal = new JLabel("Choisissez votre type de salle : ");
		typesal.setBounds(10, 50, 200, 20);
		panel.add(typesal);
		listSalle.setEditable(true);
		listSalle.setBounds(200, 50, 200, 20);
		panel.add(listSalle);

		// Number of rooms
		JLabel nbsalle = new JLabel("Nombre de salle(s) : ");
		nbsalle.setBounds(10, 100, 200, 20);
		panel.add(nbsalle);
		saisienbsalle.setBounds(200, 100, 50, 20);
		panel.add(saisienbsalle);
		saisienbsalle.setText(Integer.toString(nb));

		// Capacity of the rooms
		// Minimum
		JLabel nbpmin = new JLabel("Nombre de personne(s) minimum : ");
		nbpmin.setBounds(10, 150, 200, 20);
		panel.add(nbpmin);
		saisieminpers.setBounds(200, 150, 50, 20);
		panel.add(saisieminpers);
		saisieminpers.setText(Integer.toString(nb));

		// Maximum
		JLabel nbpers = new JLabel("Maximum : ");
		nbpers.setBounds(10, 50, 200, 20);
		panel.add(nbpers);
		saisiemaxpers.setBounds(200, 50, 50, 20);
		panel.add(saisiemaxpers);
		saisiemaxpers.setText(Integer.toString(nbm));

		// Size of the rooms
		JLabel size = new JLabel("Surface minimum de l'espace (m²) : ");
		size.setBounds(10, 150, 200, 20);
		panel.add(size);
		saisietaille.setBounds(200, 150, 50, 20);
		panel.add(saisietaille);
		saisietaille.setText(Integer.toString(nb));

		// Validation button
		JButton OK = new JButton("Valider");
		panel.add(OK);
		OK.addActionListener(this);

		// Estimated budget
		panel.add(budget);

		// Possibility to reserve through a map
		JLabel carteIHM = new JLabel("Voir l'emplacement des batiments à louer.");
		carteIHM.setForeground(Color.BLUE.darker());
		carteIHM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cp.add(carteIHM);
		carteIHM.addMouseListener(new yourListener());

		// Putting the center panel for the results of research
		add(resu, BorderLayout.CENTER);
	}

	public void showResults() {
		resu.removeAll();
		resu.validate();
		resu.repaint();

		JPanel montre = new JPanel(new GridLayout(1, 7));
		montre.setPreferredSize(new Dimension(650, 30));
		resu.add(montre);
		JLabel l1 = new JLabel("Numero ID");
		JLabel l2 = new JLabel("Type");
		JLabel l3 = new JLabel("Bâtiment");
		JLabel l4 = new JLabel("Etage");
		JLabel l5 = new JLabel("Superficie");
		JLabel l6 = new JLabel("Capacité");
		JLabel l7 = new JLabel("Votre choix");
		montre.add(l1);
		montre.add(l2);
		montre.add(l3);
		montre.add(l4);
		montre.add(l5);
		montre.add(l6);
		montre.add(l7);
		HashMap<String, List<String>> freerooms = resultat.getFreeRooms();
		List<String> list_type = freerooms.get("Type");
		List<String> list_name_b = freerooms.get("Building");
		List<String> list_floor = freerooms.get("Floor");
		List<String> list_room_id = freerooms.get("Room_id");
		List<String> list_size = freerooms.get("Size");
		List<String> list_capacity = freerooms.get("Capacity");
		String pmin = saisieminpers.getText();
		int nbpmin = Integer.parseInt(pmin);
		String pmax = saisiemaxpers.getText();
		int nbpmax = Integer.parseInt(pmax);
		String o = saisietaille.getText();
		int nbt = Integer.parseInt(o);

		for (int i = 0; i < list_room_id.size(); i++) {
			JPanel montre2 = new JPanel(new GridLayout(1, 7));
			montre2.setPreferredSize(new Dimension(650, 30));
			String capacity = list_capacity.get(i);
			String size = list_size.get(i);
			int cap = Integer.parseInt(capacity);
			int siz = Integer.parseInt(size);
			String room = list_room_id.get(i);
			String type = list_type.get(i);
			String bat = list_name_b.get(i);
			String floor = list_floor.get(i);
			if (nbpmin <= cap && nbpmax >= cap && nbt <= siz) {
				capacity += " personnes";
				size += " m²";
				JLabel r = new JLabel(room);
				JLabel t = new JLabel(type);
				JLabel b = new JLabel(bat);
				JLabel f = new JLabel(floor);
				JLabel s = new JLabel(size);
				JLabel c = new JLabel(capacity);

				montre2.add(r);
				montre2.add(t);
				montre2.add(b);
				montre2.add(f);
				montre2.add(s);
				montre2.add(c);
				button = new JButton("Réserver");
				button.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (RequestResults.ifAvailable(client, room).equals("true")) {

							Object[] options = { "Cest sûr !", "Finalement non" };

							int response = JOptionPane.showOptionDialog(null,
									"Etes-vous sûr de vouloir réserver cet espace ?", "Confirmation de réservation",
									JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

							if (response == JOptionPane.YES_OPTION) {

								// Register the reservation
								String rqIns = "insert into reserve(room_id, comp_id, valid, date_reserv) values ("
										+ room + ", (select comp_id from Company where c_name = '" + name
										+ "'), true, '" + date + "')";
								client.sendRequest(rqIns);

								// Make the room unavailable
								String rqUp = "update Room set available = false where room_id = " + room;
								client.sendRequest(rqUp);
								JOptionPane.showMessageDialog(null, "<html>La réservation de la salle n° " + room
										+ " a bien été enregistrée au " + date + " sur le compte de l'entreprise "
										+ name
										+ ". <br><center>Vous pouvez y placer des équipements via l'onglet mapping.</center>",
										"Réservation réussie", JOptionPane.INFORMATION_MESSAGE);
							} else {
								System.out.println("Refus de réservation.");
							}
						}

						else {
							JOptionPane.showMessageDialog(null, "Vous avez déjà reservé cet espace.", "Indisponibilité",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				montre2.add(button);
				resu.add(montre2);
			}
		}
	}

	public void showResultsReunion() {
		resu.removeAll();
		resu.validate();
		resu.repaint();

		JPanel montre = new JPanel(new GridLayout(1, 7));
		montre.setPreferredSize(new Dimension(650, 30));
		resu.add(montre);
		JLabel l1 = new JLabel("Numero ID");
		JLabel l2 = new JLabel("Type");
		JLabel l3 = new JLabel("Bâtiment");
		JLabel l4 = new JLabel("Etage");
		JLabel l5 = new JLabel("Superficie");
		JLabel l6 = new JLabel("Capacité");
		JLabel l7 = new JLabel("Votre choix");
		montre.add(l1);
		montre.add(l2);
		montre.add(l3);
		montre.add(l4);
		montre.add(l5);
		montre.add(l6);
		montre.add(l7);
		HashMap<String, List<String>> freerooms = resultat.getFreeReunion();
		List<String> list_type = freerooms.get("Type");
		List<String> list_name_b = freerooms.get("Building");
		List<String> list_floor = freerooms.get("Floor");
		List<String> list_room_id = freerooms.get("Room_id");
		List<String> list_size = freerooms.get("Size");
		List<String> list_capacity = freerooms.get("Capacity");
		String pmin = saisieminpers.getText();
		int nbpmin = Integer.parseInt(pmin);
		String pmax = saisiemaxpers.getText();
		int nbpmax = Integer.parseInt(pmax);
		String o = saisietaille.getText();
		int nbt = Integer.parseInt(o);

		for (int i = 0; i < list_room_id.size(); i++) {
			JPanel montre2 = new JPanel(new GridLayout(1, 7));
			montre2.setPreferredSize(new Dimension(650, 30));
			String capacity = list_capacity.get(i);
			String size = list_size.get(i);
			int cap = Integer.parseInt(capacity);
			int siz = Integer.parseInt(size);
			String room = list_room_id.get(i);
			String type = list_type.get(i);
			String bat = list_name_b.get(i);
			String floor = list_floor.get(i);
			if (nbpmin <= cap && nbpmax >= cap && nbt <= siz) {
				capacity += " personnes";
				size += " m²";
				JLabel r = new JLabel(room);
				JLabel t = new JLabel(type);
				JLabel b = new JLabel(bat);
				JLabel f = new JLabel(floor);
				JLabel s = new JLabel(size);
				JLabel c = new JLabel(capacity);

				montre2.add(r);
				montre2.add(t);
				montre2.add(b);
				montre2.add(f);
				montre2.add(s);
				montre2.add(c);
				button = new JButton("Réserver");
				button.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						if (RequestResults.ifAvailable(client, room).equals("true")) {

							Object[] options = { "Cest sûr !", "Finalement non" };

							int response = JOptionPane.showOptionDialog(null,
									"Etes-vous sûr de vouloir réserver cet espace ?", "Confirmation de réservation",
									JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

							if (response == JOptionPane.YES_OPTION) {

								// Register the reservation
								String rqIns = "insert into reserve(room_id, comp_id, valid, date_reserv) values ("
										+ room + ", (select comp_id from Company where c_name = '" + name
										+ "'), true, '" + date + "')";
								client.sendRequest(rqIns);

								// Make the room unavailable
								String rqUp = "update Room set available = false where room_id = " + room;
								client.sendRequest(rqUp);
								JOptionPane.showMessageDialog(null, "<html>La réservation de la salle n° " + room
										+ " a bien été enregistrée au " + date + " sur le compte de l'entreprise "
										+ name
										+ ". <br><center>Vous pouvez y placer des équipements via l'onglet mapping.</center>",
										"Réservation réussie", JOptionPane.INFORMATION_MESSAGE);
							} else {
								System.out.println("Refus de réservation.");
							}
						}

						else {
							JOptionPane.showMessageDialog(null, "Vous avez déjà reservé cet espace.", "Indisponibilité",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				montre2.add(button);
				resu.add(montre2);
			}
		}
	}

	public void showResultsSolo() {
		resu.removeAll();
		resu.validate();
		resu.repaint();

		JPanel montre = new JPanel(new GridLayout(1, 7));
		montre.setPreferredSize(new Dimension(650, 30));
		resu.add(montre);
		JLabel l1 = new JLabel("Numero ID");
		JLabel l2 = new JLabel("Type");
		JLabel l3 = new JLabel("Bâtiment");
		JLabel l4 = new JLabel("Etage");
		JLabel l5 = new JLabel("Superficie");
		JLabel l6 = new JLabel("Capacité");
		JLabel l7 = new JLabel("Votre choix");
		montre.add(l1);
		montre.add(l2);
		montre.add(l3);
		montre.add(l4);
		montre.add(l5);
		montre.add(l6);
		montre.add(l7);
		HashMap<String, List<String>> freerooms = resultat.getFreeSolo();
		List<String> list_type = freerooms.get("Type");
		List<String> list_name_b = freerooms.get("Building");
		List<String> list_floor = freerooms.get("Floor");
		List<String> list_room_id = freerooms.get("Room_id");
		List<String> list_size = freerooms.get("Size");
		List<String> list_capacity = freerooms.get("Capacity");
		String pmin = saisieminpers.getText();
		int nbpmin = Integer.parseInt(pmin);
		String pmax = saisiemaxpers.getText();
		int nbpmax = Integer.parseInt(pmax);
		String o = saisietaille.getText();
		int nbt = Integer.parseInt(o);

		for (int i = 0; i < list_room_id.size(); i++) {
			JPanel montre2 = new JPanel(new GridLayout(1, 7));
			montre2.setPreferredSize(new Dimension(650, 30));
			String capacity = list_capacity.get(i);
			String size = list_size.get(i);
			int cap = Integer.parseInt(capacity);
			int siz = Integer.parseInt(size);
			String room = list_room_id.get(i);
			String type = list_type.get(i);
			String bat = list_name_b.get(i);
			String floor = list_floor.get(i);
			if (nbpmin <= cap && nbpmax >= cap && nbt <= siz) {
				capacity += " personnes";
				size += " m²";
				JLabel r = new JLabel(room);
				JLabel t = new JLabel(type);
				JLabel b = new JLabel(bat);
				JLabel f = new JLabel(floor);
				JLabel s = new JLabel(size);
				JLabel c = new JLabel(capacity);

				montre2.add(r);
				montre2.add(t);
				montre2.add(b);
				montre2.add(f);
				montre2.add(s);
				montre2.add(c);
				button = new JButton("Réserver");
				button.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						if (RequestResults.ifAvailable(client, room).equals("true")) {

							Object[] options = { "Cest sûr !", "Finalement non" };

							int response = JOptionPane.showOptionDialog(null,
									"Etes-vous sûr de vouloir réserver cet espace ?", "Confirmation de réservation",
									JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

							if (response == JOptionPane.YES_OPTION) {

								// Register the reservation
								String rqIns = "insert into reserve(room_id, comp_id, valid, date_reserv) values ("
										+ room + ", (select comp_id from Company where c_name = '" + name
										+ "'), true, '" + date + "')";
								client.sendRequest(rqIns);

								// Make the room unavailable
								String rqUp = "update Room set available = false where room_id = " + room;
								client.sendRequest(rqUp);
								JOptionPane.showMessageDialog(null, "<html>La réservation de la salle n° " + room
										+ " a bien été enregistrée au " + date + " sur le compte de l'entreprise "
										+ name
										+ ". <br><center>Vous pouvez y placer jusqu'à 3 équipements via l'onglet mapping.</center>",
										"Réservation réussie", JOptionPane.INFORMATION_MESSAGE);
							} else {
								System.out.println("Refus de réservation.");
							}
						}

						else {
							JOptionPane.showMessageDialog(null, "Vous avez déjà reservé cet espace.", "Indisponibilité",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				montre2.add(button);
				resu.add(montre2);
			}
		}
	}

	public void showResultsOS() {
		resu.removeAll();
		resu.validate();
		resu.repaint();

		JPanel montre = new JPanel(new GridLayout(1, 7));
		montre.setPreferredSize(new Dimension(650, 30));
		resu.add(montre);
		JLabel l1 = new JLabel("Numero ID");
		JLabel l2 = new JLabel("Type");
		JLabel l3 = new JLabel("Bâtiment");
		JLabel l4 = new JLabel("Etage");
		JLabel l5 = new JLabel("Superficie");
		JLabel l6 = new JLabel("Capacité");
		JLabel l7 = new JLabel("Votre choix");
		montre.add(l1);
		montre.add(l2);
		montre.add(l3);
		montre.add(l4);
		montre.add(l5);
		montre.add(l6);
		montre.add(l7);
		HashMap<String, List<String>> freerooms = resultat.getFreeOS();
		List<String> list_type = freerooms.get("Type");
		List<String> list_name_b = freerooms.get("Building");
		List<String> list_floor = freerooms.get("Floor");
		List<String> list_room_id = freerooms.get("Room_id");
		List<String> list_size = freerooms.get("Size");
		List<String> list_capacity = freerooms.get("Capacity");
		String pmin = saisieminpers.getText();
		int nbpmin = Integer.parseInt(pmin);
		String pmax = saisiemaxpers.getText();
		int nbpmax = Integer.parseInt(pmax);
		String o = saisietaille.getText();
		int nbt = Integer.parseInt(o);

		for (int i = 0; i < list_room_id.size(); i++) {
			JPanel montre2 = new JPanel(new GridLayout(1, 7));
			montre2.setPreferredSize(new Dimension(650, 30));
			String capacity = list_capacity.get(i);
			String size = list_size.get(i);
			int cap = Integer.parseInt(capacity);
			int siz = Integer.parseInt(size);
			String room = list_room_id.get(i);
			String type = list_type.get(i);
			String bat = list_name_b.get(i);
			String floor = list_floor.get(i);
			if (nbpmin <= cap && nbpmax >= cap && nbt <= siz) {
				capacity += " personnes";
				size += " m²";
				JLabel r = new JLabel(room);
				JLabel t = new JLabel(type);
				JLabel b = new JLabel(bat);
				JLabel f = new JLabel(floor);
				JLabel s = new JLabel(size);
				JLabel c = new JLabel(capacity);

				montre2.add(r);
				montre2.add(t);
				montre2.add(b);
				montre2.add(f);
				montre2.add(s);
				montre2.add(c);
				button = new JButton("Réserver");
				button.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						if (RequestResults.ifAvailable(client, room).equals("true")) {

							Object[] options = { "Cest sûr !", "Finalement non" };

							int response = JOptionPane.showOptionDialog(null,
									"Etes-vous sûr de vouloir réserver cet espace ?", "Confirmation de réservation",
									JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

							if (response == JOptionPane.YES_OPTION) {

								// Register the reservation
								String rqIns = "insert into reserve(room_id, comp_id, valid, date_reserv) values ("
										+ room + ", (select comp_id from Company where c_name = '" + name
										+ "'), true, '" + date + "')";
								client.sendRequest(rqIns);

								// Make the room unavailable
								String rqUp = "update Room set available = false where room_id = " + room;
								client.sendRequest(rqUp);
								JOptionPane.showMessageDialog(null, "<html>La réservation de la salle n° " + room
										+ " a bien été enregistrée au " + date + " sur le compte de l'entreprise "
										+ name
										+ ". <br><center>Vous pouvez y placer jusqu'à 12 équipements via l'onglet mapping.</center>",
										"Réservation réussie", JOptionPane.INFORMATION_MESSAGE);
							} else {
								System.out.println("Refus de réservation.");
							}
						}

						else {
							JOptionPane.showMessageDialog(null, "Vous avez déjà reservé cet espace.", "Indisponibilité",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				montre2.add(button);
				resu.add(montre2);
			}
		}

	}

	public void actionPerformed(ActionEvent e) {

		String v = saisienbsalle.getText();
		int nbS = Integer.parseInt(v);

		if (listSalle.getSelectedItem().equals("Salle de reunion fermée")) {
			calcul = 100 * nbS;
			budget.setText("Le budget estimé est de " + calcul + " €");
			showResultsReunion();
			resu.validate();

		} else if (listSalle.getSelectedItem().equals("Espace individualisé")) {
			calcul = 50 * nbS;
			budget.setText("Le budget estimé est de " + calcul + " €");
			showResultsSolo();
			resu.validate();

		} else if (listSalle.getSelectedItem().equals("Peu importe")) {
			budget.setText("Le budget dépend du type de salle");
			showResults();
			resu.validate();

		} else {
			calcul = 200 * nbS;
			budget.setText("Le budget estimé est de " + calcul + " €");
			showResultsOS();
			resu.validate();
		}

	}

	public class yourListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			IHMBuildMap cartebat = new IHMBuildMap(client, name, id);
		}
	}

}
