package rent.digitalworkplace;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import edu.episen.si.ing1.pds.metropolis.client.ClientConfig;
import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

public class TestResearch {

	public static void main(String[] args) throws IOException, UnsupportedLookAndFeelException {
		//UIManager.setLookAndFeel(new NimbusLookAndFeel());
		String name ="Tesla";
		String id= "1";
		ClientConfig config = new ClientConfig();
        ClientCore client = new ClientCore(config);
        RequestResults resu = new RequestResults(client);
        IHMSearchLocation fenetre = new IHMSearchLocation(resu, client, name, id);
		fenetre.setVisible(true);
		fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

}
