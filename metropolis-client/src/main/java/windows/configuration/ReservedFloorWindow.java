package windows.configuration;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;
import main_page.Menu;

public class ReservedFloorWindow extends JFrame implements ActionListener, ItemListener{
	private ClientCore clientCore;
	private String nC;
	private String idC;
	private JFrame f;
	private JPanel generalPanel = new JPanel(new BorderLayout());
	
	//element for the first panel in BorderLatout.NORTH of generalPanel
	private JPanel panel1 = new JPanel();
	private JLabel bulding = new JLabel("Bâtiment :");
	private JComboBox buldingChoice = new JComboBox();
	private JLabel floor = new JLabel("Etage :");
	private JComboBox floorChoice = new JComboBox(); 
	private JButton validate = new JButton("ok");
	
	private HashMap<String,HashMap<String,HashMap<String,String>>> buildingReserved;
	private String choiceB, choiceF; // choice selected in the JComboBox
	
	//element for the second panel in BorderLayout.CENTER of generalPanel
	private JPanel panel2;
	private JLabel t;
	
	//element for the third panel in BorderLayout.SOUTH of generalPanel
	private JPanel panel3 = new JPanel();
	private JButton returnMenu = new JButton("retour");
	private JButton menu = new JButton("menu");
	
	public ReservedFloorWindow(JFrame f, ClientCore clientCore, String nC, String id){
		super();
		this.f = f;
		this.clientCore = clientCore;
		this.nC = nC;
		this.idC = id;
		this.buildingReserved = researchPlanFloor(clientCore, idC);
		
		//first panel
		panel1.setLayout(new GridLayout(1, 3));
		JPanel panel11 = new JPanel(new FlowLayout());
		panel11.add(bulding);
			//add Item to buldingChoice
		buldingChoice.addItemListener(this);
		for(String bchoice : buildingReserved.keySet()) {
			buldingChoice.addItem(bchoice);
		}
		panel11.add(buldingChoice);
		JPanel panel12 = new JPanel(new FlowLayout());
		panel12.add(floor);
		panel12.add(floorChoice);
		panel1.add(panel11);
		panel1.add(panel12);
		validate.addActionListener(this);
		panel1.add(validate);
		generalPanel.add(panel1, BorderLayout.NORTH);
		
		//second panel
		panel2=new JPanel();
		if(buildingReserved.keySet().size()<=0) {
			t = new JLabel("Vous n'avez réservé aucune salle. Pour configurer une fenêtre, veuillez d'abord réserver une salle.");
		}else {
			t = new JLabel("Choisir un batiment et un étage dans le menu déroulant");
		}
		panel2.add(t); 
		generalPanel.add(panel2, BorderLayout.CENTER);
		
		//third panel
		JLabel lb = new JLabel("EI : Espace individuel / PSDR : Petite salle de réunion / GSDR : Grande salle de réunion / OS : Open space");
		panel3.add(lb);
		returnMenu.addActionListener(this);
		panel3.add(returnMenu);
		menu.addActionListener(this);
		panel3.add(menu);
		generalPanel.add(panel3, BorderLayout.SOUTH);
		
		this.getContentPane().add(generalPanel);
		
	}
	
	public JPanel planFloor(HashMap<String,HashMap<String,HashMap<String,String>>> buildingReserved, String bRC, String fRC) {
		JPanel p = new JPanel(new GridLayout(7, 15));
		JPanel[][] colorRoom= new JPanel[7][15]; 
		
		Border lineBorder = BorderFactory.createLineBorder(Color.BLACK,1);
		for(int i=0; i<=1; i++) { // individual space
			for(int j=0; j<=9; j++) {
				colorRoom[i][j]=new JPanel();
				colorRoom[i][j].setBackground(new Color(180, 128, 177));
				colorRoom[i][j].setBorder(lineBorder);
				// name Button
				if(j==9) {  
					if(i==0) {
						JButton b = new JButton("EI"+(i+1)+i);
						b.setEnabled(false);
						colorRoom[i][j].add(b);
					}else {
						JButton b = new JButton("EI"+(i+1)+(i-1));
						b.setEnabled(false);
						colorRoom[i][j].add(b);
					}
				}else {
					JButton b = new JButton("EI"+i+(j+1));
					b.setEnabled(false);
					colorRoom[i][j].add(b);
				}
				
			}
		}
		
		for(int i=3; i<=6; i++) { // small meeting room
			for(int j=0; j<=3; j++) {
				colorRoom[i][j]=new JPanel();
				colorRoom[i][j].setBackground(Color.GREEN);
			}
		}
		JButton smr = new JButton("PSDR");
		smr.addActionListener(this);
		if(!buildingReserved.get(bRC).get(fRC).containsKey("PSDR")) {
			smr.setEnabled(false);
		}
		colorRoom[4][1].add(smr);
		
		for(int i=3; i<=6; i++) { // large meeting room
			for(int j=4; j<=9; j++) {
				colorRoom[i][j]=new JPanel();
				colorRoom[i][j].setBackground(Color.YELLOW);
			}
		}
		JButton lmr = new JButton("GSDR");
		lmr.addActionListener(this);
		if(!buildingReserved.get(bRC).get(fRC).containsKey("GSDR")) {
			lmr.setEnabled(false);
		}
		colorRoom[4][6].add(lmr);
		
		for(int i=0; i<=5; i++) { // open space
			for(int j=11; j<=14; j++) {
				colorRoom[i][j]=new JPanel();
				colorRoom[i][j].setBackground(Color.BLUE);
			}
		}
		JButton os = new JButton("OS");
		os.addActionListener(this);
		if(!buildingReserved.get(bRC).get(fRC).containsKey("OS")) {
			os.setEnabled(false);
		}
		colorRoom[3][12].add(os);
		
		for(int j=0; j<=9; j++) { // corridor1
			colorRoom[2][j]=new JPanel();
			colorRoom[2][j].setBackground(Color.LIGHT_GRAY);
		}
		for(int i=0; i<=5; i++) { // corridor2
			colorRoom[i][10]=new JPanel();
			colorRoom[i][10].setBackground(Color.LIGHT_GRAY);
		}
		for(int j=10; j<=14; j++) { // corridor3
			colorRoom[6][j]=new JPanel();
			colorRoom[6][j].setBackground(Color.LIGHT_GRAY);
		}
		for(int i=0; i<=6;i++) { //add color to p
			for(int j=0; j<=14; j++) {
				p.add(colorRoom[i][j]);
			}
		}
		return p;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		 
		String item = (String) e.getItem();
		choiceB = (String) buldingChoice.getSelectedItem();
		if(item.equals(choiceB)) {
			floorChoice.removeAllItems();
			for(String fchoice : buildingReserved.get(choiceB).keySet()) {
				floorChoice.addItem(fchoice);
			}
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
			case "ok" : 
				choiceB = (String) buldingChoice.getSelectedItem();
				choiceF = (String) floorChoice.getSelectedItem();
				//if choiceB or choiceF are not selected
				if(choiceB==null || choiceF==null) {
					JOptionPane jop = new JOptionPane();
					jop.showMessageDialog(this,"Veuillez choisir un batiment ET un étage avant de cliquer sur ok !",  "Message", JOptionPane.PLAIN_MESSAGE);
				}else {
					generalPanel.remove(panel2);
					this.getContentPane().removeAll();
					generalPanel.add(planFloor(buildingReserved, choiceB, choiceF), BorderLayout.CENTER);
					this.getContentPane().add(generalPanel);
					validate();
				}
				break; 
			case "GSDR" :
				String idRoom = buildingReserved.get(choiceB).get(choiceF).get("GSDR");
				ReservedRoomWindow rrw = new ReservedRoomWindow(this, clientCore, choiceB, choiceF, e.getActionCommand(), idRoom, nC, idC);
				this.setVisible(false);
				rrw.showWindow(900,600);
				break;
			case "PSDR" :
				String idRoom2 = buildingReserved.get(choiceB).get(choiceF).get("PSDR");
				ReservedRoomWindow rrw2 = new ReservedRoomWindow(this, clientCore, choiceB, choiceF, e.getActionCommand(), idRoom2, nC, idC);
				this.setVisible(false);
				rrw2.showWindow(900,600);
				break;
			case "OS" :
				String idRoom3 = buildingReserved.get(choiceB).get(choiceF).get("OS");
				ReservedRoomWindow rrw3 = new ReservedRoomWindow(this, clientCore, choiceB, choiceF, e.getActionCommand(), idRoom3, nC, idC);
				this.setVisible(false);
				rrw3.showWindow(900,600);
				break;
			case "retour" : 
				this.setVisible(false);
				f.setVisible(true);
				break;
			case "menu" :
				Menu m = new Menu(clientCore, nC, idC);
				this.setVisible(false);
				m.setVisible(true);
				break;
		}
		
	}
	
	public void showWindow(int width, int height) {
		this.pack();
        this.setSize(width,height);
        this.setLocationRelativeTo(null);
        this.setTitle("Configuration des fenêtres électrochromatiques - plan  de l'étage");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);
	}
	
	public HashMap<String,HashMap<String,HashMap<String,String>>> researchPlanFloor(ClientCore cc, String id) {
		String rq ="select b.b_name, f.f_name, r.room_id, r.r_name from building b ";
		rq += "inner join floors f on b.build_id=f.build_id ";
		rq += "inner join room r on f.floor_id=r.floor_id ";
		rq += "inner join reserve re on r.room_id=re.room_id ";
		rq += "where re.comp_id="+id+" and r.available=false and re.valid=true";
		
		String res = cc.sendRequest(rq).getResponseBody();
		HashMap<String,HashMap<String,HashMap<String,String>>> building = new HashMap<>();
		String[] s1 = res.split(",");
		for(int i = 0; i<s1.length; i++) {
			String[] s2 = s1[i].split("=");
			if(s2[0].equals("b_name")) {
				if(building.containsKey(s2[1])) {
					String[] fl = s1[i+1].split("=");
					String[] rid = s1[i+2].split("=");
					String[] r = s1[i+3].split("=");
					if(building.get(s2[1]).containsKey(fl[1])) {
						building.get(s2[1]).get(fl[1]).put(r[1], rid[1]);
					}else {
						HashMap<String,String> room = new HashMap<>();
						room.put(r[1], rid[1]);
						building.get(s2[1]).put(fl[1],room);
					}
				}else {
					String[] fl = s1[i+1].split("=");
					String[] rid = s1[i+2].split("=");
					String[] r = s1[i+3].split("=");
					HashMap<String,HashMap<String,String>> hmFloor = new HashMap<>();
					HashMap<String,String> room = new HashMap<>();
					room.put(r[1], rid[1]);
					hmFloor.put(fl[1], room);
					building.put(s2[1], hmFloor);
				}
			}
		}
		return building;
	}
	
}
