package windows.configuration;

import java.util.HashMap;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

public class RequestWindow {
	
	RequestWindow(){}
	
	public HashMap<String,String> dataAlgo(ClientCore cc, String id) {
		String request1 = "select * from configuration WHERE window_id="+id;
		String request2 = "select w.room_temperature, d.level_sunlight, d.outside_temperature from windows w inner join data_sensor d on w.id=d.window_id where w.id="+id;
		String data = cc.sendRequest(request1).getResponseBody();
		data += cc.sendRequest(request2).getResponseBody();
		HashMap<String,String> hm = new HashMap<>();
		
		String[] s = data.split(",");
		for(int i=0; i<s.length; i++) {
			String[] s2 = s[i].split("=");
			hm.put(s2[0], s2[1]);
		}
		return hm;
	}
	
	public HashMap<String,String> actualConfiguration(ClientCore cc, String id) {
		String data;
		String request = "select w.window_state,w.percentage_blind,w.percentage_opacity,w.room_temperature,d.outside_temperature,d.level_sunlight from windows w inner join data_sensor d on w.id=d.window_id where w.id="+id;
		data = cc.sendRequest(request).getResponseBody();
		HashMap<String,String> hm = new HashMap<>();
		
		String[] s = data.split(",");
		for(int i=0; i<s.length; i++) {
			String[] s2 = s[i].split("=");
			hm.put(s2[0], s2[1]);
		}
		return hm;
	}

}
