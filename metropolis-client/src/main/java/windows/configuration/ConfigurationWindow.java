package windows.configuration;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

public class ConfigurationWindow extends JFrame implements ActionListener{
	// constante for the size of the JFrame
	public static final int WIDTH = 540;
	public static final int HEIGHT = 650;
	
	private String window_id; // id of the window in the data bases
	RequestWindow requestWindow = new RequestWindow();
	ClientCore clientCore;
	
	//element for the first panel in BorderLayout.NORTH of the JFrame
	private JPanel panel1;
	private JTextArea label;
	private JButton refresh;
	
	private String blind, window, opacityW, tempO, tempI, levelSunshine;
	
	//element for the second panel in BorderLayout.CENTER of the JFrame
	private JTabbedPane tab;
	private JPanel windowTab, blindTab, brightnessTab, temperatureTab;
	private JSlider slide;
	
	//element for the third panel in BorderLayout.SOUTH of the JFrame
	private JPanel panel3;
	private ButtonGroup groupWindow, groupBlind, groupOpacity, groupTem;
		//text to be filled in for the configuration of the store
	private JTextArea tb1, tb2, tb3, tb4, tb5;
		//text to be filled in for the configuration of the opacity
	private JTextArea to1, to2, to3, to4, to5;
	private JButton cancel;
	private JButton confirm;

	ConfigurationWindow(ClientCore cc, String nameRoom, String nameWindow, String id) {
		//the JFrame
		super("Configuration de la "+ nameWindow + " de la salle "+nameRoom);
		this.setSize(WIDTH,HEIGHT);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(new BorderLayout());
		//this.getContentPane().setBackground(Color.CYAN);
		
		this.clientCore = cc;
		this.window_id = id;
		
		//first part : actual configuration - the first panel
		panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		//update the window data in the database
		HashMap<String,String> h = requestWindow.dataAlgo(clientCore, window_id);
		AlgoWindow algo = new AlgoWindow(clientCore, h);
		algo.algoWindow();
		//text
		HashMap<String,String> windowStateConfig = requestWindow.actualConfiguration(clientCore, window_id);
		String actualConfiguration = windowInformation(windowStateConfig);
		label= new JTextArea(actualConfiguration);
		label.setEditable(false);
		panel1.add(label);
		//button refresh
		refresh = new JButton("actualiser");
		refresh.addActionListener(this);
		refresh.setBackground(Color.WHITE);
		panel1.add(refresh);
		//add panel1 to this
		this.getContentPane().add(panel1, BorderLayout.NORTH);
		
		//element for the sencond panel - configuration element
		tab = new JTabbedPane();
		windowTab = panelWindowTab();
		blindTab = panelBlindTab(); 
		brightnessTab = panelBrightnessTab();
		temperatureTab = panelTemperatureTab();
		
		tab.add("fenêtre", windowTab);
		tab.add("store", blindTab);
		tab.add("teinte", brightnessTab);
		tab.add("température", temperatureTab);
		this.getContentPane().add(tab, BorderLayout.CENTER);
		
		//element for the third panel
		panel3 = new JPanel();
		cancel = new JButton("annuler");
		confirm = new JButton("confirmer");
		cancel.addActionListener(this);
		confirm.addActionListener(this);
		panel3.add(confirm);
		panel3.add(cancel);
		this.getContentPane().add(panel3, BorderLayout.SOUTH);		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
			case "actualiser" :
				//update the window data in the database
				HashMap<String,String> h = requestWindow.dataAlgo(clientCore, window_id);
				AlgoWindow algo = new AlgoWindow(clientCore, h);
				algo.algoWindow();
				
				//update the text in the label
				HashMap<String,String> windowStateConfig = requestWindow.actualConfiguration(this.clientCore, this.window_id);
				String t = windowInformation(windowStateConfig);
				this.label.setEditable(true);
				this.label.setText(t);
				this.label.setEditable(false);
				break;
				
			case "annuler" :
				this.dispose();
				break;
				
			case "confirmer" :
				String requestUpdateConfig = dataChoice(); 
				if(requestUpdateConfig.substring(0, 1).equals("E")) { // case where the parameters are badly defined 
					JOptionPane jop = new JOptionPane();
					jop.showMessageDialog(this, requestUpdateConfig, "Message", JOptionPane.PLAIN_MESSAGE);
					
				}else { // case where the parameters are well defined
					String res = clientCore.sendRequest(requestUpdateConfig).getResponseBody();
					if(res.equals("ok")) {
						//message
						JOptionPane jop = new JOptionPane();
						jop.showMessageDialog(this, "Votre configuration a bien été prise en compte", "Message", JOptionPane.PLAIN_MESSAGE);
						
						//update the window data in the database
						HashMap<String,String> hm = requestWindow.dataAlgo(clientCore, window_id);
						AlgoWindow algoW = new AlgoWindow(clientCore, hm);
						algoW.algoWindow();
						
						//update the text in the label
						HashMap<String,String> windowStateConfig2 = requestWindow.actualConfiguration(this.clientCore, this.window_id);
						String tW = windowInformation(windowStateConfig2);
						this.label.setEditable(true);
						this.label.setText(tW);
						this.label.setEditable(false);
					}
				}
				break;
		}
		
	}
	
	// method to do the panel windowTab
	public JPanel panelWindowTab() {
		JPanel p = new JPanel(new GridLayout(0, 1));
		groupWindow = new ButtonGroup();
		JLabel label = new JLabel("Choisir un choix pour l'ouverture de la fenêtre : ");
		JRadioButton r1 = new JRadioButton("fenêtre fermé", true);
		r1.setActionCommand("fermé");
		JRadioButton r2 = new JRadioButton("fenêtre entre ouverte");
		r2.setActionCommand("entre ouverte");
		JRadioButton r3 = new JRadioButton("fenêtre ouverte");
		r3.setActionCommand("ouverte");
		groupWindow.add(r1);
		groupWindow.add(r2);
		groupWindow.add(r3);
		p.add(label);
		p.add(r1);
		p.add(r2);
		p.add(r3);
		
		return p;
	}
	// method to do the panel blindTab
	public JPanel panelBlindTab() {
		JPanel general = new JPanel(new GridLayout(0, 1));
		groupBlind = new ButtonGroup();
		JLabel label = new JLabel("Choisir un choix pour le volet de la fenêtre : ");
		//first choice
		JRadioButton r1 = new JRadioButton("maintenir le volet fermer à un certain pourcentage", true);
		r1.setActionCommand("choix1");
		groupBlind.add(r1);
			JPanel p1 = new JPanel(new FlowLayout());
			JLabel labelr1 = new JLabel("entré une valeur entre 0 et 100 :");
			tb1 = new JTextArea("100");
			p1.add(labelr1);
			p1.add(tb1);
		//second choice
		JRadioButton r2 = new JRadioButton("fermé le volet lorsque le capteur de la fenêtre relève une certaine valeur");
		r2.setActionCommand("choix2");
		groupBlind.add(r2);
			JPanel p2 = new JPanel(new GridLayout(0, 1));
				JPanel p11 = new JPanel();
					JLabel l11 = new JLabel("1 - A partir de quel valeur le store commence à se fermer (en W/m2, valeur positive) :");
					tb2 = new JTextArea("200");
					p11.add(l11);
					p11.add(tb2);
				JPanel p12 = new JPanel();
					JLabel l12 = new JLabel("a quel pourcentage (entre 0 et 100):");
					tb3 = new JTextArea("5");
					p12.add(l12);
					p12.add(tb3);
				JPanel p13 = new JPanel();
					JLabel l13 = new JLabel("2 - Lorsque cette valeur augmente de (valeur positive):");
					tb4 = new JTextArea("20");
					p13.add(l13);
					p13.add(tb4);
				JPanel p14 = new JPanel();
					JLabel l14 = new JLabel("on augmente la fermeture du store de + (en % entre 0 et 100) :");
					tb5 = new JTextArea("5");
					p14.add(l14);
					p14.add(tb5);
				p2.add(p11);
				p2.add(p12);
				p2.add(p13);
				p2.add(p14);
		general.add(label);
		general.add(r2);
		general.add(p2);
		general.add(r1);
		general.add(p1);
		
		return general;
	}
	
	public JPanel panelBrightnessTab() {
		JPanel general = new JPanel(new GridLayout(0,1));
		groupOpacity = new ButtonGroup();
		JLabel label = new JLabel("Choisir un choix pour la teinte de la fenêtre : ");
		// first choice
		JRadioButton r1 = new JRadioButton("Teinté automatiquement par rapport au niveau d'ensoleillement", true);
		r1.setActionCommand("choix2");
		groupOpacity.add(r1);
			JPanel p1 = new JPanel(new GridLayout(0, 1));
				JPanel p11 = new JPanel();
					JLabel l11 = new JLabel("1 - A partir de quel valeur la vitre commence à ce teinté (en W/m2, valeur positive) :");
					to2 = new JTextArea("200");
					p11.add(l11);
					p11.add(to2);
				JPanel p12 = new JPanel();
					JLabel l12 = new JLabel("a quel pourcentage (entre 0 et 100) :");
					to3 = new JTextArea("5");
					p12.add(l12);
					p12.add(to3);
				JPanel p13 = new JPanel();
					JLabel l13 = new JLabel("2 - Lorsque cette valeur augmente de (valeur positive):");
					to4 = new JTextArea("20");
					p13.add(l13);
					p13.add(to4);
				JPanel p14 = new JPanel();
					JLabel l14 = new JLabel("on augmente la teinte de la vitre de + (en %, entre 0 et 100) :");
					to5 = new JTextArea("5");
					p14.add(l14);
					p14.add(to5);
				p1.add(p11);
				p1.add(p12);
				p1.add(p13);
				p1.add(p14);
				
		// second choice
		JRadioButton r2 = new JRadioButton("Maintenir une teinte constante de la fenêtre");
		r2.setActionCommand("choix1");
		groupOpacity.add(r2);
			JPanel p2 = new JPanel();
				JLabel l2 = new JLabel("Choisir la teinte en % (pas teinté=0, maximum de la teinte=100) :");
				to1 = new JTextArea("50");
				p2.add(l2);
				p2.add(to1);
		general.add(label);
		general.add(r1);
		general.add(p1);
		general.add(r2);
		general.add(p2);
		return general;
	}
	// method to do the panel temperatureTab
		public JPanel panelTemperatureTab() {
			JPanel general = new JPanel(new BorderLayout());
			groupTem = new ButtonGroup();
			JLabel label = new JLabel("Choisir un choix pour la température de la salle : ");
			// first choice
			JRadioButton r1 = new JRadioButton("choix 1", true);
			r1.setActionCommand("choix2");
			groupTem.add(r1);
				JPanel panelr1 = new JPanel(new GridLayout(0,1));
				String newLine = System.getProperty("line.separator");
				String choix1 = "Permettre que les parramètres précédemment choisi puisse être modifié le temps " +newLine
						+ "que la température de salle soit à la température idéale lorsque cela est possible." +newLine
						+ "Par exemple, si à l'extérieur il fait plus chaud et qu'à l'intérieur la température " +newLine
						+ "est trop basse alors la fenêtre sera ouverte. Choisir la temperature idéale de la salle :";
				JTextArea textr1 = new JTextArea(choix1);
				textr1.setEditable(false);
				slide = new JSlider();
				slide.setMaximum(22);
			    slide.setMinimum(18);
			    slide.setValue(20);
			    slide.setPaintTicks(true);
			    slide.setPaintLabels(true);
			    slide.setMinorTickSpacing(1);
			    slide.setMajorTickSpacing(1);
			    panelr1.add(r1);
				panelr1.add(textr1);
				panelr1.add(slide);
			//second choice
			JRadioButton r2 = new JRadioButton("choix 2");
			r2.setActionCommand("choix1");
			groupTem.add(r2);
				JTextArea textr2 = new JTextArea("Ne pas changer les paramettre précedament choisi pour réguler la température");
				textr2.setEditable(false);
				panelr1.add(r2);
				panelr1.add(textr2);
			general.add(label, BorderLayout.NORTH);
			general.add(panelr1, BorderLayout.CENTER);
			return general;
		}
	
	public String windowInformation(HashMap<String,String> windowSC) {
		this.window = windowSC.get("window_state");
		this.blind = windowSC.get("percentage_blind");
		this.opacityW = windowSC.get("percentage_opacity");
		this.tempI = windowSC.get("room_temperature");
		this.tempO = windowSC.get("outside_temperature");
		this.levelSunshine = windowSC.get("level_sunlight");
		
		String newLine = System.getProperty("line.separator");
		String actualConfiguration = "La configuration actuelle :"+newLine;
		actualConfiguration += "La fenêtre est "+this.window+newLine;
		actualConfiguration += "Le store est fermé à "+this.blind+"%"+newLine;
		actualConfiguration += "La fenêtre est teinté à "+this.opacityW+"%"+newLine;
		actualConfiguration += "La température intérieur est de "+this.tempI+"°C"+newLine;
		actualConfiguration += "La température exterieur est de "+this.tempO+"°C"+newLine;
		actualConfiguration += "Le niveau d'ensoleillement est de "+this.levelSunshine+"W/m2"+newLine;
		
		return actualConfiguration;
	}
	
	public String dataChoice() {
		String request="update configuration SET ";
		String newLine = System.getProperty("line.separator");
		String message = "Erreur, veuillez vérifier les paramétres :"+newLine;
		boolean bol = true;
		
		String window_state = groupWindow.getSelection().getActionCommand();
		String blind_choice = groupBlind.getSelection().getActionCommand();
		String opacity_choice = groupOpacity.getSelection().getActionCommand();
		String temperature_choice = groupTem.getSelection().getActionCommand();
		
		String one_percentage;
		String blind_level_start;
		String blind_percentage_start;
		String blind_level_add;
		String blind_percentage_add;
		
		String one_opacity;
		String opacity_level_start;
		String opacity_percentage_start;
		String opacity_level_add;
		String opacity_percentage_add;
		
		String ideal_temperature;
		
		if(blind_choice.equals("choix1")) {
			one_percentage = tb1.getText();
			//check that the percentage is between 0 and 100
			if(Integer.parseInt(one_percentage)<0 || Integer.parseInt(one_percentage)>100) {
				bol = false;
				message += "Dans l'onglet store une valeur est mal renseignée."+newLine;
			}
			blind_level_start = "-1";
			blind_percentage_start = "-1";
			blind_level_add = "-1";
			blind_percentage_add = "-1";
		}else {
			one_percentage = "-1";
			blind_level_start = tb2.getText();
			blind_percentage_start = tb3.getText();
			blind_level_add = tb4.getText();
			blind_percentage_add = tb5.getText();
			if(Integer.parseInt(blind_level_start)<0 || Integer.parseInt(blind_percentage_start)<0 || Integer.parseInt(blind_level_add)<0 || Integer.parseInt(blind_percentage_add)<0 || Integer.parseInt(blind_percentage_start)>100 || Integer.parseInt(blind_percentage_add)>100) {
				bol = false;
				message += "Dans l'onglet store une valeur est mal renseignée."+newLine;
			}
		}
		
		if(opacity_choice.equals("choix1")) {
			one_opacity = to1.getText();
			if(Integer.parseInt(one_opacity)<0 || Integer.parseInt(one_opacity)>100) {
				bol = false;
				message += "Dans l'onglet teinte une valeur est mal renseignée."+newLine;
			}
			opacity_level_start = "-1";
			opacity_percentage_start = "-1";
			opacity_level_add = "-1";
			opacity_percentage_add = "-1";
		}else {
			one_opacity = "-1";
			opacity_level_start = to2.getText();
			opacity_percentage_start = to3.getText();
			opacity_level_add = to4.getText();
			opacity_percentage_add = to5.getText();
			if(Integer.parseInt(opacity_level_start)<0 || Integer.parseInt(opacity_percentage_start)<0 || Integer.parseInt(opacity_level_add)<0 || Integer.parseInt(opacity_percentage_add)<0 || Integer.parseInt(opacity_percentage_start)>100 || Integer.parseInt(opacity_percentage_add)>100) {
				bol = false;
				message += "Dans l'onglet teinte une valeur est mal renseignée."+newLine;
			}
		}
		
		if(temperature_choice.equals("choix2")) {
			ideal_temperature = ""+slide.getValue();
		}else {
			ideal_temperature = "-1";
		}
		
		request += "window_state='"+window_state+"', blind_choice='"+blind_choice+"', one_percentage="+one_percentage+", blind_level_start="+blind_level_start+", blind_percentage_start="+blind_percentage_start+", blind_level_add="+blind_level_add+", blind_percentage_add="+blind_percentage_add+", ";
		request += "opacity_choice='"+opacity_choice+"', one_opacity="+one_opacity+", opacity_level_start="+opacity_level_start+", opacity_percentage_start="+opacity_percentage_start+", opacity_level_add="+opacity_level_add+", opacity_percentage_add="+opacity_percentage_add;
		request += ", temperature_choice='"+temperature_choice+"', ideal_temperature="+ideal_temperature;
		request+=" where window_id="+window_id;
		
		if(bol) {
			return request;
		}else {
			return message;
		}
		
		
	}
}
