package windows.configuration;

import java.util.HashMap;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

public class AlgoWindow {
	ClientCore cc;
	
	String window_id;
	
	String window_state;
	String blind_choice;
	String opacity_choice;
	String temperature_choice;
	
	String one_percentage;
	int blind_level_start;
	int blind_percentage_start;
	int blind_level_add;
	int blind_percentage_add;
	
	String one_opacity;
	int opacity_level_start;
	int opacity_percentage_start;
	int opacity_level_add;
	int opacity_percentage_add;
	
	int ideal_temperature;
	
	//data_sensor
	int outside_temperature, level_sunlight, room_temperature;
	
	//window state to send
	private String window, blind, opacityW, tempI;
	
	public AlgoWindow(ClientCore cc, HashMap<String,String> h){
		this.cc = cc;
		
		HashMap<String,String> hm = h;
		window_id = hm.get("id");
		window_state = hm.get("window_state");
		blind_choice = hm.get("blind_choice");
		opacity_choice = hm.get("opacity_choice");
		temperature_choice = hm.get("temperature_choice");
		
		one_percentage = hm.get("one_percentage");
		blind_level_start = Integer.parseInt(hm.get("blind_level_start"));
		blind_percentage_start = Integer.parseInt(hm.get("blind_percentage_start"));
		blind_level_add = Integer.parseInt(hm.get("blind_level_add"));
		blind_percentage_add = Integer.parseInt(hm.get("blind_percentage_add"));
		
		one_opacity = hm.get("one_opacity");
		opacity_level_start = Integer.parseInt(hm.get("opacity_level_start"));
		opacity_percentage_start = Integer.parseInt(hm.get("opacity_percentage_start"));
		opacity_level_add = Integer.parseInt(hm.get("opacity_level_add"));
		opacity_percentage_add = Integer.parseInt(hm.get("opacity_percentage_add"));
		
		ideal_temperature = Integer.parseInt(hm.get("ideal_temperature"));
		
		room_temperature = Integer.parseInt(hm.get("room_temperature"));
		outside_temperature = Integer.parseInt(hm.get("outside_temperature"));
		level_sunlight = Integer.parseInt(hm.get("level_sunlight"));
		
	}
	
	public void algoWindow() {
		// String window
		this.window = "'"+window_state+"'";
		
		//String blind
		if(blind_choice.equals("choix1")) {
			this.blind = one_percentage;
		}
		if(blind_choice.equals("choix2")) {
			if(level_sunlight<blind_level_start) {
				this.blind = "0";
			}else {
				this.blind = "100";
				int countPercentage = blind_percentage_start;
				int countSunlight = blind_level_start;
				boolean bool = false;
				while(countPercentage<100 || !bool) { 
					if(level_sunlight>=countSunlight && level_sunlight<countSunlight+blind_level_add) {
						bool = true;
						this.blind = ""+countPercentage;
					}
					countPercentage += blind_percentage_add;
					countSunlight += blind_level_add;
				}
			}
		}
		
		//String opacityW
		if(opacity_choice.equals("choix1")) {
			this.opacityW = one_opacity;
		}
		if(opacity_choice.equals("choix2")) {
			if(level_sunlight<opacity_level_start) {
				this.opacityW = "0";
			}else {
				this.opacityW = "100";
				int countPercentage = opacity_percentage_start;
				int countSunlight = opacity_level_start;
				boolean bool = false;
				while(countPercentage<100 || !bool) { 
					if(level_sunlight>=countSunlight && level_sunlight<countSunlight+opacity_level_add) {
						bool = true;
						this.opacityW = ""+countPercentage;
					}
					countPercentage += opacity_percentage_add;
					countSunlight += opacity_level_add;
				}
			}
		}
		
		//String tempI 
		if(temperature_choice.equals("choix1")) {
			this.tempI = ""+room_temperature;
		}
		if(temperature_choice.equals("choix2")) {
			this.tempI = ""+room_temperature;
			if(ideal_temperature<room_temperature && room_temperature<outside_temperature) {
				this.window = "'fermé'"; //
				this.blind = "100";
				this.tempI = ""+room_temperature;
			}
			if(room_temperature<ideal_temperature && ideal_temperature<outside_temperature) {
				if((outside_temperature-room_temperature)<6) {
					this.window = "'entre ouverte'";
				}
				if((outside_temperature-room_temperature)>5) {
					this.window = "'ouverte'";
				}
				this.blind = "0";
				if(Integer.parseInt(this.opacityW)>50) {
					this.opacityW = "50";
				}
				this.tempI = ""+ideal_temperature;
			}
			if(ideal_temperature<=outside_temperature && outside_temperature<room_temperature) {
				if((room_temperature-outside_temperature)<6) {
					this.window = "'entre ouverte'";
				}
				if((room_temperature-outside_temperature)>5) {
					this.window = "'ouverte'";
				}
				this.blind = "0";
				if(Integer.parseInt(this.opacityW)>50) {
					this.opacityW = "50";
				}
				this.tempI = ""+outside_temperature;
			}
			if(outside_temperature<=ideal_temperature && ideal_temperature<room_temperature) {
				if((room_temperature-outside_temperature)<6) {
					this.window = "'entre ouverte'";
				}
				if((room_temperature-outside_temperature)>5) {
					this.window = "'ouverte'";
				}
				this.blind = "0";
				if(Integer.parseInt(this.opacityW)>50) {
					this.opacityW = "50";
				}
				this.tempI = ""+ideal_temperature;
			}
		}
		
		String requestU = "update windows set window_state="+this.window+",percentage_blind="+this.blind+",percentage_opacity="+this.opacityW+", room_temperature="+this.tempI+" where id="+window_id;
		cc.sendRequest(requestU);
	}

}
