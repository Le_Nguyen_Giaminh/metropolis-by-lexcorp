package windows.configuration;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;
import main_page.Menu;

public class ReservedRoomWindow extends JFrame implements ActionListener {
	private ClientCore clientCore;
	private JFrame f;
	private String nameRoom;
	private String idRoom;
	private String nC;
	private String idC;
	private HashMap<String,String> windowsMap;
	private JPanel generalPanel = new JPanel(new BorderLayout());
	
	private JPanel panel1 = new JPanel(new GridLayout(0,1));
	
	private JPanel panel2;
	
	private JPanel panel3 = new JPanel();
	private JButton returnMenu = new JButton("retour");
	private JButton menu = new JButton("menu");
	
	ReservedRoomWindow(JFrame f, ClientCore clientCore,String nB, String nF, String name, String id, String nC, String idC){
		this.f=f;
		this.clientCore = clientCore;
		this.nameRoom = name;
		this.idRoom = id;
		this.nC = nC;
		this.idC = idC;
		this.windowsMap = researchWindowMapping(clientCore, idRoom);
		
		//first panel
		panel1.add(new JLabel("Nom du batiment : "+nB+"/ Nom de l'étage : "+nF+"/ Plan de la salle : "+nameRoom));
		if(windowsMap.keySet().size()<=0) {
			panel1.add(new JLabel("Les fenêtres électrochromatiques n'ont pas été mappées ou la fonctionalité n'a pas été demandée lors de la réservation."));
		}
		generalPanel.add(panel1, BorderLayout.NORTH);
		
		//second panel
		switch(nameRoom) {
			case "OS" :
				panel2 = planRoomOS();
				break;
			case "GSDR" :
				panel2 = planRoomGSDR();
				break;
			case "PSDR" :
				panel2 = planRoomPSDR();
				break;
		}
		generalPanel.add(panel2, BorderLayout.CENTER);
		
		//third panel
		returnMenu.addActionListener(this);
		panel3.add(returnMenu);
		menu.addActionListener(this);
		panel3.add(menu);
		generalPanel.add(panel3, BorderLayout.SOUTH);
		
		this.getContentPane().add(generalPanel);
	}
	
	public JPanel planRoomOS() { 
		JPanel p = new JPanel(new GridLayout(6, 4));
		Border lineBorder = BorderFactory.createLineBorder(Color.BLACK,3);
		p.setBorder(lineBorder);
		JPanel[][] colorRoom= new JPanel[6][4];
		for(int i=0; i<6; i++) {
			for(int j=0; j<4; j++) {
				colorRoom[i][j] = new JPanel(new BorderLayout());
				colorRoom[i][j].setBackground(Color.BLUE);
				if(i==0) {
					JButton b = new JButton("fenêtre"+(j+1));
					b.addActionListener(this);
					if(!windowsMap.containsKey(""+j)) { // if window is not mapped
						b.setEnabled(false); 
					}
					colorRoom[i][j].add(b, BorderLayout.NORTH);
				}
				p.add(colorRoom[i][j]);
			}
		}
		return p;
	}
	
	public JPanel planRoomPSDR() {
		JPanel p = new JPanel(new GridLayout(4, 4));
		Border lineBorder = BorderFactory.createLineBorder(Color.BLACK,3);
		p.setBorder(lineBorder);
		JPanel[][] colorRoom= new JPanel[4][4];
		for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				colorRoom[i][j] = new JPanel(new BorderLayout());
				colorRoom[i][j].setBackground(Color.GREEN);
				p.add(colorRoom[i][j]);
			}
		}
		JButton b = new JButton("fenêtre1");
		b.addActionListener(this);
		if(!windowsMap.containsKey("2")) {// if window is not mapped
			b.setEnabled(false); 
		}
		colorRoom[3][2].add(b, BorderLayout.SOUTH);
		return p;
		}
	
	public JPanel planRoomGSDR() { 
		JPanel p = new JPanel(new GridLayout(4, 6));
		Border lineBorder = BorderFactory.createLineBorder(Color.BLACK,3);
		p.setBorder(lineBorder);
		JPanel[][] colorRoom= new JPanel[4][6];
		for(int i=0; i<4; i++) {
			for(int j=0; j<6; j++) {
				colorRoom[i][j] = new JPanel(new BorderLayout());
				colorRoom[i][j].setBackground(Color.YELLOW);
				p.add(colorRoom[i][j]);
			}
		}
		JButton b1 = new JButton("fenêtre1");
		b1.addActionListener(this);
		if(!windowsMap.containsKey("1")) { // if window is not mapped
			b1.setEnabled(false);
		}
		colorRoom[3][1].add(b1, BorderLayout.SOUTH);
		JButton b2 = new JButton("fenêtre2");
		b2.addActionListener(this);
		if(!windowsMap.containsKey("4")) { // if window is not mapped
			b2.setEnabled(false);
		}
		colorRoom[3][4].add(b2, BorderLayout.SOUTH);
		return p;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		case "retour" : 
			this.setVisible(false);
			f.setVisible(true); //TODO refresh
			break;
		case "menu" :
			Menu m = new Menu(clientCore, nC, idC);
			this.setVisible(false);
			m.setVisible(true);
			break;
		default : 
			switch(nameRoom) {
				case "OS" :
					for(int i=1; i<5; i++) {
						if(e.getActionCommand().equals("fenêtre"+i)) {
							String idWindow = windowsMap.get(""+(i-1));
							ConfigurationWindow w2 = new ConfigurationWindow(clientCore, nameRoom, e.getActionCommand(), idWindow);
							w2.setVisible(true);
						}
					}
					break;
				case "GSDR" :
					if(e.getActionCommand().equals("fenêtre1")) {
						String idWindow = windowsMap.get("1");
						ConfigurationWindow w2 = new ConfigurationWindow(clientCore, nameRoom, e.getActionCommand(), idWindow);
						w2.setVisible(true);
					}
					if(e.getActionCommand().equals("fenêtre2")) {
						String idWindow = windowsMap.get("4");
						ConfigurationWindow w2 = new ConfigurationWindow(clientCore, nameRoom, e.getActionCommand(), idWindow);
						w2.setVisible(true);
					}
					break;
				case "PSDR" :
					if(e.getActionCommand().equals("fenêtre1")) {
						String idWindow = windowsMap.get("2");
						ConfigurationWindow w2 = new ConfigurationWindow(clientCore, nameRoom, e.getActionCommand(), idWindow);
						w2.setVisible(true);
					}
					break;
		}
			break;
		}
		
	}
	
	public HashMap<String,String> researchWindowMapping(ClientCore clientCore, String id) {
		String request = "select id,position_y from windows where room_id="+id+" and is_reserved=true";
		String response = clientCore.sendRequest(request).getResponseBody();
		HashMap<String,String> hm = new HashMap<>();
		String[] s1 = response.split(",");
		for(int i=0; i<s1.length; i++) {
			String[] s2 = s1[i].split("=");
			if(s2[0].equals("id")) {
				String[] window_y = s1[i+1].split("=");
				hm.put(window_y[1], s2[1]);
			}
		}
		return hm;
	}

	public void showWindow(int width, int height) {
		this.pack();
        this.setSize(width,height);
        this.setLocationRelativeTo(null);
        this.setTitle("Configuration des fenêtres électrochromatiques - plan  de la salle");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setVisible(true);

	}

}
