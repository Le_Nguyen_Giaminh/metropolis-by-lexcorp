package edu.episen.si.ing1.pds.metropolis.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ClientConfig {
    private final static Logger logger = LoggerFactory.getLogger(ClientConfig.class.getName());
    private static final String pdsClientConfigEnvVar = "PDS_CLIENT_CONFIG";
    private final String clientConfigFileLocation;
    private ClientCoreConfig config;

    public ClientConfig() throws IOException {
        clientConfigFileLocation = System.getenv(pdsClientConfigEnvVar);
        logger.debug("Config file = {}", pdsClientConfigEnvVar);

        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        config = mapper.readValue(new File(clientConfigFileLocation), ClientCoreConfig.class);
        logger.debug("Config = {}", config.toString());
    }

    public ClientCoreConfig getConfig() {
        return config;
    }
}
