package edu.episen.si.ing1.pds.metropolis.client;

public class ClientCoreConfig {
    public int listenPort;
    public int timeOut;

    public ClientCoreConfig() {
    }

    public int getListenPort() {
        return listenPort;
    }

    public int getTimeOut() {
        return timeOut;
    }

    public void setListenPort(int listenPort) {
        this.listenPort = listenPort;
    }

    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

    @Override
    public String toString() {
        return "ServerCoreConfig{" +
                "listenPort=" + listenPort +
                ", timeOut=" + timeOut +
                '}';
    }
}
