package edu.episen.si.ing1.pds.metropolis.client;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "response")
public class ResponseForm {

    public String requestId;
    public String responseBody;

    public String getRequestId() {
        return requestId;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setResponseBody(String response) {
        this.responseBody = response;
    }

    @Override
    public String toString() {
        return "{" +
                "requestId=" + requestId +
                ", responseBody=" + responseBody +
                '}';
    }
}
