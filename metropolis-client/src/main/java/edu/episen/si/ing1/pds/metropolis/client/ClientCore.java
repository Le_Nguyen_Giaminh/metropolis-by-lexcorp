package edu.episen.si.ing1.pds.metropolis.client;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.base.Splitter;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class ClientCore {
	private static Logger logger = LoggerFactory.getLogger(ClientCore.class.getName());
	private static int countRequest = 0;
	private ClientConfig config;

	public ClientCore(ClientConfig config) throws IOException{
		this.config = config;
	}

	public ResponseForm sendRequest(String op) {
		try {
			// test with local host
			//InetAddress ipAddress = InetAddress.getLocalHost();
		   // Socket socket = new Socket(ipAddress, config.getConfig().getListenPort());

			// test with VM
			Socket socket = new Socket("172.31.254.90" ,config.getConfig().getListenPort());

			OutputStream outputStream = socket.getOutputStream();
			InputStream inputStream = socket.getInputStream();
			
			final ObjectMapper mapperForJson= new ObjectMapper();
            RequestForm rq = new RequestForm();
			countRequest++;
  			rq.setRequestId(Integer.toString(countRequest));
			rq.setRequestContent(op);

			logger.info("Request to send to the server: " + mapperForJson.writeValueAsString(rq));
			byte[] b = mapperForJson.writeValueAsBytes(rq);
			DataOutputStream dos = new DataOutputStream(outputStream);
			if (b.length > 0) {
				dos.write(b, 0, b.length);
			}
			
			while (inputStream.available()== 0) {
				Thread.sleep(100);
			}
				
			
			byte[] inputData = new byte[inputStream.available()];
			inputStream.read(inputData);

			final ObjectMapper mapper = new ObjectMapper(new JsonFactory());
			ResponseForm responseForm = mapper.readValue(inputData, ResponseForm.class);
			String id = responseForm.getRequestId();
			String rp = responseForm.getResponseBody();
			
			logger.info("The request ID is : " + id + "\n The response is : " + rp);
			socket.close();
			return responseForm;
		 } catch (Exception e) {
			 e.printStackTrace();
			throw new RuntimeException("Stop immediately");
		}
	}
}