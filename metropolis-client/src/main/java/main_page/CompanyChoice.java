package main_page;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

public class CompanyChoice extends JFrame implements ActionListener{
	private ClientCore clientCore;
	HashMap<String,String> list = new HashMap<String,String>();
	String nameC = "";
	String idC = "";
	private JLabel company = new JLabel("Choisir une entreprise");
	private JComboBox companyChoice;
	private JButton validate = new JButton("ok");
	private JPanel p = new JPanel(new BorderLayout());
	
	public CompanyChoice(ClientCore clientCore){
		super("Choisir une entreprise");
		
		this.clientCore = clientCore;
		this.list = researchCompany(clientCore);
		
		JPanel p1 = new JPanel();
		p1.add(company);
		String[] choices = new String[list.size()];
		int i=0;
		for(String s : list.keySet()) { 
			choices[i] = s;
			i++;
		} 
		Arrays.sort(choices);
		companyChoice = new JComboBox<>(choices);
		p1.add(companyChoice);
		validate.addActionListener(this);
		p1.add(validate);
		p.add(p1, BorderLayout.CENTER);
		this.add(p);

		this.pack();
		this.setSize(400,300);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("ok")) {
			nameC = (String) companyChoice.getSelectedItem();
			for(String s : list.keySet()) {
				if(s.equals(nameC)) {
					idC = list.get(nameC);
				}
			} 
			
			Menu m = new Menu(clientCore,nameC,idC);
			this.dispose();
			m.setVisible(true);
		}
		
	}
	
	public HashMap<String,String> researchCompany(ClientCore clientCore) {
		String request = "select comp_id, c_name from company";
		String response = clientCore.sendRequest(request).getResponseBody();
		HashMap<String,String> listComp = new HashMap<String,String>();
		String[] s1 = response.split(",");
		for(int i=0; i<s1.length; i++) {
			String[] s2 = s1[i].split("=");
			if(s2[0].equals("comp_id")) {
				String[] nameComp = s1[i+1].split("=");
				listComp.put(nameComp[1], s2[1]);
			}
		}
		return listComp;
	}

}
