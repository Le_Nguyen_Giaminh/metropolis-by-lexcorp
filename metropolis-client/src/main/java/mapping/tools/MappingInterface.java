package mapping.tools;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.checkerframework.checker.units.qual.s;

import mapping.windows.Rooms;
import java.awt.event.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.awt.*;

public class MappingInterface extends JFrame implements ActionListener {
    
    private Rooms room;
    private int width, length, roomID;
    private RoomTools tools;

    private JPanel generalPanel, panel1, panel2, panel3, panel4, panel5;

    private JLabel l1 = new JLabel("Choisir un type d'équipement à mapper :");
    private JComboBox typeChoice;
    private JButton validate = new JButton("Valider");
    private JButton mapper = new JButton("Mapper");
    private JButton returnButton = new JButton("Retour"); 

    private String selectedType;
    private MySpot[][] cells;
    private MySpot cell_to_be_map;

    public MappingInterface(Rooms room, RoomTools tools, int id, MySpot[][] s) {
        this.room = room;
        this.tools = tools;
        roomID = id;
        width = s.length;
        length = s[0].length;
        cells = new MySpot[width][length];
        for(int i=0; i<width; i++) {
            for(int j=0; j<length; j++) {
                cells[i][j] = s[i][j].clone();
            }
        }
        setLayout();
    }

    public void setLayout() {
        generalPanel = new JPanel(new BorderLayout());

        //panel 1
        panel1 = new JPanel();
        panel1.setLayout(new GridLayout(1, 3));
        panel5 = new JPanel(new FlowLayout());
        String[] choices = tools.getEquipmentType(roomID);
		Arrays.sort(choices);
        typeChoice = new JComboBox(choices);
        typeChoice.addActionListener(this);
        panel5.add(l1);
        panel5.add(typeChoice);
        panel5.add(validate);
        panel1.add(panel5);
        validate.setEnabled(false);
        validate.addActionListener(this);

        //panel 2
        panel2 = new JPanel();
        panel2.add(returnButton);
        returnButton.addActionListener(this);

        //plan of the room
        panel3 = new JPanel();
        panel3.setLayout(new GridLayout(width,length));
        for(int i=0; i<width; i++) {
            for(int j=0; j<length; j++) {
                panel3.add(cells[i][j]);
                if(cells[i][j].getTag().equals("d")) {
                    cells[i][j].setBackground(Color.yellow);
                } else {
                    cells[i][j].setBackground(Color.gray);
                }
                cells[i][j].addActionListener(this);
                cells[i][j].setEnabled(false);
            }
        }

        //panel 4
        panel4 = new JPanel(new BorderLayout());
        panel4.add(mapper, BorderLayout.SOUTH);
        mapper.addActionListener(this);
        mapper.setEnabled(false);
        generalPanel.add(panel1, BorderLayout.NORTH);
        generalPanel.add(panel2, BorderLayout.PAGE_END);
        generalPanel.add(panel3, BorderLayout.CENTER);
        generalPanel.add(panel4, BorderLayout.LINE_END);
        this.setContentPane(generalPanel);
    }

    public void updateCompatibleSpot(){
        cell_to_be_map = null;
        for(int i=0; i<width; i++) {
            for(int j=0; j<length; j++) {
                panel3.add(cells[i][j]);
                if(cells[i][j].getTag().equals("d")) {
                    cells[i][j].setBackground(Color.yellow);
                } else {
                    cells[i][j].setBackground(Color.gray);
                }
                cells[i][j].setEnabled(false);
            }
        }
        HashMap<String,List<Integer>> compatible = tools.getPositionOfCompatibleSpot(roomID, selectedType);
        List<Integer> list_x = compatible.get("x");
        List<Integer> list_y = compatible.get("y");
        for(int i=0; i<list_x.size();i++) {
            cells[list_x.get(i)][list_y.get(i)].setBackground(new Color(99, 73, 174));
            cells[list_x.get(i)][list_y.get(i)].setEnabled(true);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == returnButton){
            room.setVisible(true);
            this.dispose();
        } else if(e.getSource() == typeChoice) {
            selectedType = typeChoice.getSelectedItem().toString();
            validate.setEnabled(true);
        } else if(e.getSource() == validate) {
            int nb = tools.getAvailableQuantity(selectedType);
            JOptionPane jop = new JOptionPane();
            if(nb==0) {
                jop.showMessageDialog(this, "Il n'y a plus de " + selectedType + " en stock. Veuillez supprimer un avant de continuer.",  "Warning", JOptionPane.PLAIN_MESSAGE);
            }
            else {
                updateCompatibleSpot();
            }
        } else if(e.getSource() instanceof MySpot) {
            if(cell_to_be_map!=null){
                cell_to_be_map.setBackground(new Color(99, 73, 174));
            }
            cell_to_be_map = (MySpot)e.getSource();
            cell_to_be_map.setBackground(new Color(188, 121, 222));
            mapper.setEnabled(true);
        } else if(e.getSource() == mapper) {
            Integer[] e_ids = tools.getAvailableEquipmentID(selectedType);
            Arrays.sort(e_ids);
            int eID = (Integer)JOptionPane.showInputDialog(this,"Choisir l'ID de l'équipement à installer : ",selectedType,JOptionPane.QUESTION_MESSAGE,null,e_ids,e_ids[0]);
            tools.installEquipment(eID, roomID, cell_to_be_map.getI(), cell_to_be_map.getJ());
            room.updateNewlyMappedEquipment(eID, cell_to_be_map.getI(), cell_to_be_map.getJ(), selectedType);
            room.setVisible(true);
            this.dispose();
        }
    }

    public void showWindow() {
        this.pack();
        this.setSize(900,600);
        this.setLocationRelativeTo(null);
        this.setTitle("");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(true);
        this.setVisible(true);
    }
}
