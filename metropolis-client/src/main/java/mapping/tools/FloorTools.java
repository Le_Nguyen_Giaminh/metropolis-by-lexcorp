package mapping.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

public class FloorTools {

    private ClientCore client;

    public FloorTools(ClientCore cc) {
        client = cc;
    }
    
    public ClientCore getClient() {
        return client;
    }
    
    public String[] getBuildingList() {
        String rq = "select b_name from building order by b_name ASC";        
        String rp = client.sendRequest(rq).getResponseBody();
        String[] s1 = rp.split(",");
        String[] res = new String[s1.length];
        for(int i=0; i<s1.length; i++) {
            res[i] = s1[i].split("=")[1];
        }
        return res;
    }

    public String[] getFloorList(String building_name) {
        String rq = "select f_name from floors f left join building b on f.build_id=b.build_id where b_name='"+building_name+"' order by f_name ASC";        
        String rp = client.sendRequest(rq).getResponseBody();
        String[] s1 = rp.split(",");
        String[] res = new String[s1.length];
        for(int i=0; i<s1.length; i++) {
            res[i] = s1[i].split("=")[1];
        }
        return res;
    }

    public int getFloorID(String buildingName, String floorName) {
        String rq = "select f.floor_id from floors f left join building b on f.build_id=b.build_id where b_name='"+buildingName+"' and f_name='"+floorName+"'";
        String rp = client.sendRequest(rq).getResponseBody();
        String[] s = rp.split(",")[0].split("=");
        return Integer.parseInt(s[1]);
    }

    public HashMap<String,List<String>> getReservedRooms(int floorID) {
        String rq = "select ro.room_id, ro.r_name, ro.type, re.comp_id from room ro left join reserve re on ro.room_id=re.room_id where valid=true and ro.floor_id=" + floorID;
        String rp = client.sendRequest(rq).getResponseBody();
        HashMap<String,List<String>> res = new HashMap<>();
        
        String[] s1 = rp.split(",");
        List<String> list_id = new ArrayList<>();
        List<String> list_name = new ArrayList<>();
        List<String> list_type = new ArrayList<>();
        List<String> list_comp_id = new ArrayList<>();

        for(int i=0; i<s1.length; i++) {
            String[] s2 = s1[i].split("=");
            if(s2[0].equals("room_id")) {
                list_id.add(s2[1]);
            }
            else if (s2[0].equals("r_name")){
                list_name.add(s2[1]);
            }
            else if (s2[0].equals("type")){
                list_type.add(s2[1]);
            }
            else if (s2[0].equals("comp_id")){
                list_comp_id.add(s2[1]);
            }
        }

        res.put("id", list_id);
        res.put("name", list_name);
        res.put("type", list_type);
        res.put("company", list_comp_id);
        return res;
    }
}
