package mapping.tools;

import javax.swing.JButton;

public class MySpot extends JButton {

    private int i;
    private int j;
    private String id="-1";
    private String name="null";
    private String tag; // there are 4 tags : c for couloir, d for disponible, t for fonctionne, f for hors-service

    public MySpot(int x, int y, String tag) {
        this.i  = x;
        this.j = y;
        this.tag = tag;
    }

    public void setID(String id) {
        this.id = id;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public void removeID() {
        id = null;
    }

    public String getID() {
        return id;
    }

    public MySpot clone() {
        MySpot s = new MySpot(i, j, tag);
        s.setID(id);
        s.setName(name);
        return s;
    }

    public int getI() {
        return i;
    }

    public int getJ(){
        return j;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
