package mapping.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.episen.si.ing1.pds.metropolis.client.ClientCore;

public class RoomTools {

    private ClientCore client;
    
    public RoomTools(ClientCore cc) {
        client = cc;
    }

    public boolean checkEmptyRoom(int roomID) {
        String rq = "select i.equipment_id, e.description, s.position_x, s.position_y, i.state from spot_for_equipment s inner join installed_equipment i on s.spot_id = i.spot_id inner join equipment e on i.equipment_id = e.equipment_id where s.room_id = " + roomID;
        String rp = client.sendRequest(rq).getResponseBody();
        if(rp.equals("")){
            return true;
        }
        else {
            return false;
        }
    }

    public HashMap<String,List<String>> getOccupiedSpace(int roomID) {
        String rq = "select i.equipment_id, e.description, s.position_x, s.position_y, i.state from spot_for_equipment s inner join installed_equipment i on s.spot_id = i.spot_id inner join equipment e on i.equipment_id = e.equipment_id where s.room_id = " + roomID;
        String rp = client.sendRequest(rq).getResponseBody();
        HashMap<String,List<String>> res = new HashMap<>();
        
        String[] s1 = rp.split(",");
        List<String> list_id = new ArrayList<>();
        List<String> list_name = new ArrayList<>();
        List<String> list_x = new ArrayList<>();
        List<String> list_y = new ArrayList<>();
        List<String> list_state = new ArrayList<>();

        for(int i=0; i<s1.length; i++) {
            String[] s2 = s1[i].split("=");
            if(s2[0].equals("position_x")) {
                list_x.add(s2[1]);
            }
            else if (s2[0].equals("position_y")){
                list_y.add(s2[1]);
            }
            else if (s2[0].equals("equipment_id")){
                list_id.add(s2[1]);
            }
            else if (s2[0].equals("description")){
                list_name.add(s2[1]);
            }
            else {
                list_state.add(s2[1]);
            }
        }

        res.put("id", list_id);
        res.put("name", list_name);
        res.put("x", list_x);
        res.put("y", list_y);
        res.put("state", list_state);
        return res;
    }

    public HashMap<String,List<String>> getWindows(int roomID) {
        String rq = "select id, is_reserved, position_x, position_y from windows where room_id=" + roomID;
        String rp = client.sendRequest(rq).getResponseBody();
        HashMap<String,List<String>> res = new HashMap<>();
        
        String[] s1 = rp.split(",");
        List<String> list_id = new ArrayList<>();
        List<String> list_state = new ArrayList<>();
        List<String> list_x = new ArrayList<>();
        List<String> list_y = new ArrayList<>();

        for(int i=0; i<s1.length; i++) {
            String[] s2 = s1[i].split("=");
            if(s2[0].equals("position_x")) {
                list_x.add(s2[1]);
            }
            else if (s2[0].equals("position_y")){
                list_y.add(s2[1]);
            }
            else if (s2[0].equals("id")){
                list_id.add(s2[1]);
            }
            else if (s2[0].equals("is_reserved")){
                list_state.add(s2[1]);
            }
        }

        res.put("id", list_id);
        res.put("x", list_x);
        res.put("y", list_y);
        res.put("state", list_state);
        return res;
    }

    public HashMap<String,List<Integer>> getFreeSpace(int roomID) {
        String rq = "select position_x, position_y from spot_for_equipment s left join installed_equipment i on s.spot_id = i.spot_id where i.spot_id is null and s.room_id = " + roomID;
        String rp = client.sendRequest(rq).getResponseBody();
        HashMap<String,List<Integer>> res = new HashMap<>();
        
        String[] s1 = rp.split(",");
        List<Integer> list_x = new ArrayList<>();
        List<Integer> list_y = new ArrayList<>();
        for(int i=0; i<s1.length; i++) {
            String[] s2 = s1[i].split("=");
            if(s2[0].equals("position_x")) {
                list_x.add(Integer.parseInt(s2[1]));
            }
            else {
                list_y.add(Integer.parseInt(s2[1]));
            }
        }
        res.put("x", list_x);
        res.put("y", list_y);
        return res;
    }

    public boolean checkIfFull(int roomID) {
        String rq = "select count(*) from spot_for_equipment s left join installed_equipment i on s.spot_id = i.spot_id where i.spot_id is null and s.room_id ="+roomID;
        String rp = client.sendRequest(rq).getResponseBody();
        String[] s = rp.split(",")[0].split("=");
        if(Integer.parseInt(s[1])==0) {
            return true;
        } 
        else {
            return false;
        }
    }

    public String[] getEquipmentType(int roomID) {
        String rq = "select distinct t.name from type_of_equipment t left join compatibility c on t.equipment_type_id=c.equipment_type_id left join spot_for_equipment s on s.spot_type_id=c.spot_type_id left join installed_equipment i on s.spot_id = i.spot_id where i.spot_id is null and s.room_id="+roomID;        
        String rp = client.sendRequest(rq).getResponseBody();
        String[] s1 = rp.split(",");
        String[] res = new String[s1.length];
        for(int i=0; i<s1.length; i++) {
            res[i] = s1[i].split("=")[1];
        }
        return res;
    }

    public int getAvailableQuantity(String equipmentType) {
        String rq = "select count(e.equipment_id) from equipment e left join installed_equipment i on e.equipment_id=i.equipment_id where i.equipment_id is null and e.description='" + equipmentType + "'";
        String rp = client.sendRequest(rq).getResponseBody();
        String[] s = rp.split(",")[0].split("=");
        return Integer.parseInt(s[1]);
    }

    public HashMap<String,List<Integer>> getPositionOfCompatibleSpot(int roomID, String equipementType) {
        String rq = "select s.position_x, s.position_y from spot_for_equipment s left join compatibility c on s.spot_type_id=c.spot_type_id left join type_of_equipment t on t.equipment_type_id=c.equipment_type_id left join installed_equipment i on s.spot_id = i.spot_id where i.spot_id is null and s.room_id =" + roomID +  "and t.name='" + equipementType + "'";
        String rp = client.sendRequest(rq).getResponseBody();
        HashMap<String,List<Integer>> res = new HashMap<>();
        String[] s1 = rp.split(",");
        List<Integer> list_x = new ArrayList<>();
        List<Integer> list_y = new ArrayList<>();
        for(int i=0; i<s1.length; i++) {
            String[] s2 = s1[i].split("=");
            if(s2[0].equals("position_x")) {
                list_x.add(Integer.parseInt(s2[1]));
            }
            else {
                list_y.add(Integer.parseInt(s2[1]));
            }
        }
        res.put("x", list_x);
        res.put("y", list_y);
        return res;
    }

    public Integer[] getAvailableEquipmentID(String equipment) {
        String rq = "select e.equipment_id from equipment e left join installed_equipment i on e.equipment_id=i.equipment_id where i.equipment_id is null and e.description='"+ equipment+"'";
        String rp = client.sendRequest(rq).getResponseBody();
        String[] s1 = rp.split(",");
        Integer[] res = new Integer[s1.length];
        for(int i=0; i<s1.length; i++) {
            res[i] = Integer.parseInt(s1[i].split("=")[1]);
        }
        return res;
    }

    public void installEquipment(int idequipment, int roomID, int x, int y) {
        String rq1 = "select s.spot_id from spot_for_equipment s where s.position_x="+x+" and s.position_y="+y+" and room_id="+roomID;
        String rp = client.sendRequest(rq1).getResponseBody();
        String[] s = rp.split(",")[0].split("=");
        int idspot = Integer.parseInt(s[1]);
        String rq2 = "insert into installed_equipment(spot_id,equipment_id,state) values ("+idspot+","+idequipment+",true)";
        client.sendRequest(rq2);
    }

    public void removeEquipment(int idequipment){
        String rq = "delete from installed_equipment where equipment_id=" + idequipment;
        client.sendRequest(rq);
    }

    public void updateState(int idequipment, String state) {
        String rq = "update installed_equipment set state=" + state + " where equipment_id=" + idequipment;
        client.sendRequest(rq);
    }

    public void updateWindowState(int idWindow, String state) {
        String rq = "update windows set is_reserved=" + state + " where id=" + idWindow;
        client.sendRequest(rq);
    }
}
