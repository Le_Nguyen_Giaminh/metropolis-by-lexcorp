package mapping.tools;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import mapping.windows.Rooms;

import java.awt.*;
import java.awt.event.*;

public class RemoveInterface extends JFrame implements ActionListener {
    private Rooms room;
    private int width, length;
    private RoomTools tools;

    private JPanel generalPanel, panel1, panel2, panel3, panel4;

    private JLabel l1 = new JLabel("Choisir un équipement à supprimer :");
    private JButton remove = new JButton("Supprimer");
    private JButton returnButton = new JButton("Retour"); 

    private MySpot[][] cells;
    private MySpot cell_to_be_remove;

    public RemoveInterface(Rooms room, RoomTools tools, MySpot[][] s) {
        this.room = room;
        this.tools = tools;
        width = s.length;
        length = s[0].length;
        cells = new MySpot[width][length];
        for(int i=0; i<width; i++) {
            for(int j=0; j<length; j++) {
                cells[i][j] = s[i][j].clone();
            }
        }
        setLayout();
    }

    public void setLayout() {
        generalPanel = new JPanel(new BorderLayout());

        //panel 1
        panel1 = new JPanel();
        panel1.setLayout(new GridLayout(1, 3));
        panel1.add(l1);
    
        //panel 2
        panel2 = new JPanel();
        panel2.add(returnButton);
        returnButton.addActionListener(this);

        //plan of the room
        panel3 = new JPanel();
        panel3.setLayout(new GridLayout(width,length));
        for(int i=0; i<width; i++) {
            for(int j=0; j<length; j++) {
                panel3.add(cells[i][j]);
                if(cells[i][j].getTag().equals("t")) {
                    cells[i][j].setBackground(Color.green);
                    cells[i][j].setEnabled(true);
                    cells[i][j].setText(cells[i][j].getName() + " " + cells[i][j].getID());
                } 
                else if(cells[i][j].getTag().equals("f")) {
                    cells[i][j].setBackground(Color.red);
                    cells[i][j].setEnabled(true);
                    cells[i][j].setText(cells[i][j].getName()+ " " + cells[i][j].getID());
                }
                else {
                    cells[i][j].setBackground(Color.gray);
                    cells[i][j].setEnabled(false);
                }
                cells[i][j].addActionListener(this);
            }
        }

        panel4 = new JPanel(new BorderLayout());
        panel4.add(remove, BorderLayout.SOUTH);
        remove.addActionListener(this);
        remove.setEnabled(false);
        generalPanel.add(panel1, BorderLayout.NORTH);
        generalPanel.add(panel2, BorderLayout.PAGE_END);
        generalPanel.add(panel3, BorderLayout.CENTER);
        generalPanel.add(panel4, BorderLayout.LINE_END);
        this.setContentPane(generalPanel);
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == returnButton){
            room.setVisible(true);
            this.dispose();
        }
        else if(e.getSource() instanceof MySpot) {
            cell_to_be_remove = (MySpot)e.getSource();
            cell_to_be_remove.setBackground(Color.WHITE);
            remove.setEnabled(true);
        }
        else if(e.getSource() == remove) {
            tools.removeEquipment(Integer.parseInt(cell_to_be_remove.getID()));
            room.updateNewlyRemovedEquipment(cell_to_be_remove.getI(), cell_to_be_remove.getJ());
            room.setVisible(true);
            this.dispose();
        }
    }

    public void showWindow() {
        this.pack();
        this.setSize(900,600);
        this.setLocationRelativeTo(null);
        this.setTitle("");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(true);
        this.setVisible(true);
    }
}
