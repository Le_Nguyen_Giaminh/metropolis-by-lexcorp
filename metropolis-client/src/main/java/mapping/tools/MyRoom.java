package mapping.tools;

import javax.swing.JButton;

public class MyRoom extends JButton {
    private String name;
    private String type;
    private int room_id;
    private String status; // there are 3 : t for rooms reserved by this company, f for rooms reserved by another company and d for available rooms

    public MyRoom(String name, String type, String status) {
        this.name = name;
        this.type = type;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setRoomID(int id) {
        room_id = id;
    }

    public void setStatus(String s) {
        status = s;
    }

    public String getStatus() {
        return status;
    }

    public int getRoomID() {
        return room_id;
    }

    public String getType() {
        return type;
    }
}
