package mapping.windows;

import mapping.tools.RoomTools;

public class IndividualSpace extends Rooms {

    public IndividualSpace(RoomTools t, int id, String bat_name, String f_name, Floor fw) {
        super(t,3,2, id, bat_name, f_name,fw);
        cells1[2][1].setText("l'entrée");
    }
}
