package mapping.windows;

import mapping.tools.RoomTools;

public class SmallMeetingRoom extends Rooms {

    public SmallMeetingRoom(RoomTools t, int id, String bat_name, String f_name, Floor fw) {
        super(t,4,4, id,bat_name, f_name, fw);
        cells1[0][1].setText("l'entrée");
    }
}
