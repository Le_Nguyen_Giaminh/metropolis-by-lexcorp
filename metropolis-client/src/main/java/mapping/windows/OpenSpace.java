package mapping.windows;

import mapping.tools.RoomTools;

public class OpenSpace extends Rooms {

    public OpenSpace(RoomTools t, int id, String bat_name, String f_name, Floor fw) {
        super(t,6,4, id,bat_name, f_name, fw);
        cells1[2][0].setText("l'entrée");
        cells1[5][2].setText("l'entrée");
    }

}
