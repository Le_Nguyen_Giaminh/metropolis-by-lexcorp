package mapping.windows;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import main_page.Menu;
import mapping.tools.FloorTools;
import mapping.tools.MyRoom;
import mapping.tools.RoomTools;

public class Floor extends JFrame implements ActionListener {

	private FloorTools tools;

    private JPanel generalPanel,panel1, panel11, panel3, panel4;
	private JLabel bulding = new JLabel("Bâtiment :");
	private JLabel floor = new JLabel("Etage :");

	private JComboBox<String> buildingChoice;
	private JComboBox<String> floorChoice;

	private JLabel lb1, lb2, lb3, lb4;

	private JButton validate = new JButton("Valider");
	private JButton chooseAnother = new JButton("Annuler");
	private JButton menu = new JButton("Menu"); 

	private int company_id, floor_id;
	private String company_name, building_name, floor_name;

	private MyRoom os, psdr, gsdr;
	private MyRoom[] ei_list = new MyRoom[20];
	
	
	public Floor(FloorTools tools, int company_id, String company_name){
		this.tools = tools;
		this.company_id = company_id;
		this.company_name = company_name;
		buildingChoice = new JComboBox<>(tools.getBuildingList());
		floorChoice = new JComboBox<>();
		os = new MyRoom("OS", "OS","d");
		psdr = new MyRoom("PSDR","PSDR","d");
		gsdr = new MyRoom("GSDR","GSDR","d");
		for(int i=1; i<=20; i++) {
			ei_list[i-1] = new MyRoom("EI" + i, "EI","d");
			ei_list[i-1].setText(ei_list[i-1].getName());
		}
		panel4 = new JPanel();
		setLayout();
		String[] f_list = tools.getFloorList("Batiment A");
		for (int i=0; i<f_list.length; i++) {
			floorChoice.addItem(f_list[i]);
		};
		validate.setEnabled(false);
		validate.addActionListener(this);
		chooseAnother.addActionListener(this);
		floorChoice.setEnabled(false);
		chooseAnother.setEnabled(false);
	}

	public void setLayout() {
		generalPanel = new JPanel(new BorderLayout());

		//panel 1
		panel1 = new JPanel();
		panel1.setLayout(new GridLayout(1, 4));

		panel11 = new JPanel(new FlowLayout());
		panel11.add(bulding);
		panel11.add(buildingChoice);
		panel11.add(floor);
		panel11.add(floorChoice);
		panel11.add(validate);
		panel11.add(chooseAnother);
		panel1.add(panel11);
		
		buildingChoice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				building_name = buildingChoice.getSelectedItem().toString();
				buildingChoice.setEnabled(false);
				floorChoice.setEnabled(true);
				validate.setEnabled(false);
				chooseAnother.setEnabled(true);
			}
		});
		floorChoice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				validate.setEnabled(true);
				floor_name = (String)floorChoice.getSelectedItem();
			}
		});
		generalPanel.add(panel1, BorderLayout.NORTH);
		
		//panel 3
		panel3 = new JPanel();
        lb1 = new JLabel("EI : Espace individuel / ");
        lb2 = new JLabel("PSDR : Petite salle de réunion / ");
        lb3 = new JLabel("GSDR : Grande salle de réunion / ");
        lb4 = new JLabel("OS : Open space");
        panel3.add(lb1);
        panel3.add(lb2);
        panel3.add(lb3);
        panel3.add(lb4);
		
		menu.addActionListener(this);
		panel3.add(menu);
		generalPanel.add(panel3, BorderLayout.SOUTH);
        generalPanel.setBorder(new TitledBorder("Choisir une salle pour visualiser son plan des équipments"));
		setContentPane(generalPanel);
	}

	public JPanel setFloorPlan(){
		// panel 2 - floor plan
		JPanel panel2 = new JPanel(new GridLayout(7, 15));
		JPanel[][] colorRoom= new JPanel[7][15]; 
        
		Border lineBorder = BorderFactory.createLineBorder(Color.BLACK,1);
        panel2.setBorder(new TitledBorder("Plan de " + floor_name + " - " + building_name));

		int cpt = 0;
		for(int i=0; i<=1; i++) { // individual spaces
			for(int j=0; j<=9; j++) {
				colorRoom[i][j]=new JPanel();
				colorRoom[i][j].setBackground(new Color(180, 128, 177));
				colorRoom[i][j].setBorder(lineBorder);
				colorRoom[i][j].add(ei_list[cpt]);
				ei_list[cpt].setBackground(Color.yellow);
				ei_list[cpt].addActionListener(this);
				cpt++;
			}
		}

		for(int i=3; i<=6; i++) { // small meeting room
			for(int j=0; j<=3; j++) {
				colorRoom[i][j]=new JPanel();
				colorRoom[i][j].setBackground(new Color(122, 206, 93));
			}
		}
		colorRoom[4][1].add(psdr);
		psdr.setText(psdr.getName());
		psdr.setBackground(Color.yellow);
		psdr.addActionListener(this);

		for(int i=3; i<=6; i++) { // large meeting room
			for(int j=4; j<=9; j++) {
				colorRoom[i][j]=new JPanel();
				colorRoom[i][j].setBackground(new Color(237, 229, 113));
			}
		}
		colorRoom[4][6].add(gsdr);
		gsdr.setText(gsdr.getName());
		gsdr.setBackground(Color.yellow);
		gsdr.addActionListener(this);

		for(int i=0; i<=5; i++) { // open space
			for(int j=11; j<=14; j++) {
				colorRoom[i][j]=new JPanel();
				colorRoom[i][j].setBackground(new Color(46, 127, 245));
			}
		}
        
        colorRoom[3][12].add(os);
		os.setText(os.getName());
		os.setBackground(Color.yellow);
		os.addActionListener(this);

		for(int j=0; j<=9; j++) { // corridor1
			colorRoom[2][j]=new JPanel();
			colorRoom[2][j].setBackground(Color.LIGHT_GRAY);
		}
		for(int i=0; i<=5; i++) { // corridor2
			colorRoom[i][10]=new JPanel();
			colorRoom[i][10].setBackground(Color.LIGHT_GRAY);
		}
		for(int j=10; j<=14; j++) { // corridor3
			colorRoom[6][j]=new JPanel();
			colorRoom[6][j].setBackground(Color.LIGHT_GRAY);
		}
		for(int i=0; i<=6;i++) { 
			for(int j=0; j<=14; j++) {
				panel2.add(colorRoom[i][j]);
			}
		}

		return panel2;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == validate) {
			generalPanel.remove(panel4);
			resetAllRoom();
			floor_id = tools.getFloorID(building_name, floor_name);
			panel4 = setFloorPlan();
			updateReservedRooms();
			generalPanel.add(panel4, BorderLayout.CENTER);
			setContentPane(generalPanel);
			revalidate();
			repaint();
			buildingChoice.setEnabled(false);
			chooseAnother.setEnabled(true);
			floorChoice.setEnabled(true);

		} else if(e.getSource() == chooseAnother) {
			resetAllRoom();
			setLayout();
			buildingChoice.setEnabled(true);
			floorChoice.setEnabled(false);
			validate.setEnabled(false);
			chooseAnother.setEnabled(false);
			revalidate();
		} else if(e.getSource() instanceof MyRoom) {
			MyRoom r = (MyRoom)e.getSource();
			if(r.getStatus().equals("d")) {
				JOptionPane jop = new JOptionPane();
				jop.showMessageDialog(this,"Cette salle est disponible, vous pouvez la réserver à partir de l'onglet Réservation",  "Message", JOptionPane.PLAIN_MESSAGE);
			} else if(r.getStatus().equals("f")){
				JOptionPane jop = new JOptionPane();
				jop.showMessageDialog(this,"Une autre entreprise a déjà réservé cette salle !",  "Message", JOptionPane.PLAIN_MESSAGE);
			}
			else {
				JOptionPane jop = new JOptionPane();
				jop.showMessageDialog(this,"Yay ! Cette salle est à vous. Je vais vous montrer sa carte !",  "Message", JOptionPane.PLAIN_MESSAGE);
				if(r.getType().equals("PSDR")) {
					SmallMeetingRoom w = new SmallMeetingRoom(new RoomTools(tools.getClient()),r.getRoomID(),building_name, floor_name, this);
					this.setVisible(false);
        			w.showWindow();
				}
				else if(r.getType().equals("GSDR")) {
					BigMeetingRoom w = new BigMeetingRoom(new RoomTools(tools.getClient()),r.getRoomID(),building_name, floor_name,this);
					this.setVisible(false);
        			w.showWindow();
				}
				else if(r.getType().equals("OS")) {
					OpenSpace w = new OpenSpace(new RoomTools(tools.getClient()),r.getRoomID(),building_name, floor_name,this);
					this.setVisible(false);
        			w.showWindow();
				}
				else {
					IndividualSpace w = new IndividualSpace(new RoomTools(tools.getClient()),r.getRoomID(),building_name, floor_name,this);
					this.setVisible(false);
        			w.showWindow();
				}
			}
		} else if(e.getSource() ==  menu) {
			Menu menuPage = new Menu(tools.getClient(), company_name, String.valueOf(company_id));
			menuPage.setVisible(true);
			this.dispose();
		}
	}

	public void resetAllRoom(){
		os = new MyRoom("OS", "OS","d");
		os.setText(os.getName());
		os.setBackground(Color.yellow);

		psdr = new MyRoom("PSDR","PSDR","d");
		psdr.setText(psdr.getName());
		psdr.setBackground(Color.yellow);

		gsdr = new MyRoom("GSDR","GSDR","d");
		gsdr.setText(gsdr.getName());
		gsdr.setBackground(Color.yellow);
		for(int i=1; i<=20; i++) {
			ei_list[i-1] = new MyRoom("EI" + i, "EI","d");
			ei_list[i-1].setText(ei_list[i-1].getName());
			ei_list[i-1].setBackground(Color.yellow);
		}
	}

	public void updateReservedRooms(){
		HashMap<String,List<String>> rooms = tools.getReservedRooms(floor_id);
		List<String> list_id = rooms.get("id");
        List<String> list_name = rooms.get("name");
        List<String> list_type = rooms.get("type");
        List<String> list_comp_id = rooms.get("company");

		for(int i=0; i<list_id.size(); i++){
			String cID = list_comp_id.get(i);
			if(list_type.get(i).equals("PSDR")) {	
				psdr.setRoomID(Integer.parseInt(list_id.get(i)));
				if(cID.equals(String.valueOf(company_id))) {
					psdr.setBackground(Color.green);
					psdr.setStatus("t");
				} 
				else {
					psdr.setBackground(Color.red);
					psdr.setStatus("f");
				}
			}
			else if(list_type.get(i).equals("GSDR")) {
				gsdr.setRoomID(Integer.parseInt(list_id.get(i)));
				if(cID.equals(String.valueOf(company_id))) {
					gsdr.setBackground(Color.green);
					gsdr.setStatus("t");
				} 
				else {
					gsdr.setBackground(Color.red);
					gsdr.setStatus("f");
				}
			}
			else if(list_type.get(i).equals("OS")){
				os.setRoomID(Integer.parseInt(list_id.get(i)));
				if(cID.equals(String.valueOf(company_id))) {
					os.setBackground(Color.green);
					os.setStatus("t");
				} 
				else {
					os.setBackground(Color.red);
				}
			}
			else if(list_type.get(i).equals("EI")){
				String roomName = list_name.get(i);
				int n = Integer.parseInt(roomName.substring(2,roomName.length()));
				ei_list[n-1].setRoomID(Integer.parseInt(list_id.get(i)));
				if(cID.equals(String.valueOf(company_id))) {
					ei_list[n-1].setBackground(Color.green);
					ei_list[n-1].setStatus("t");
				} 
				else {
					ei_list[n-1].setBackground(Color.red);
					ei_list[n-1].setStatus("f");
				}
			}
		}
	}

    public void showWindow() {
        this.pack();
        this.setSize(900,600);
        this.setLocationRelativeTo(null);
        this.setTitle("");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(true);
        this.setVisible(true);
    }
}
