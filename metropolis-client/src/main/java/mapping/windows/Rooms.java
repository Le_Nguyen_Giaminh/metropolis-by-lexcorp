package mapping.windows;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import mapping.tools.MappingInterface;
import mapping.tools.MySpot;
import mapping.tools.RemoveInterface;
import mapping.tools.RoomTools;

import java.awt.event.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Rooms extends JFrame implements ActionListener {
    
    protected Floor previous;
    protected RoomTools tools;
    protected int roomID;
    protected String b_name, floor_name;

    protected JPanel generalPanel, panel1, panel2, panel3;
    protected JLabel l1 = new JLabel("Cliquez sur la case pour changer l'état de l'équipement");
    protected JLabel l2 = new JLabel("Cliquez sur la case pour changer l'état de la fenêtre");

    protected JButton showEquipments = new JButton("Voir les équipements"); 
    protected JButton showWindows = new JButton("Voir les fenetres"); 
    protected JButton removeEquipments = new JButton("Supprimer les équipements"); 
    protected JButton mapping = new JButton("Mapper les équipements"); 
    protected JButton returnButton = new JButton("Retour"); 

    protected MySpot[][] cells1, cells2;

    protected int width, length;
    protected boolean empty, full;

    public Rooms(RoomTools t, int width, int length, int id, String b_name, String floor_name, Floor fw){
        previous = fw;
        roomID = id;
        tools = t;
        this.b_name = b_name;
        this.floor_name = floor_name;
        this.width = width;
        this.length = length;
        cells1 = new MySpot[width][length];
        cells2 = new MySpot[width][length];
        empty = tools.checkEmptyRoom(roomID);
        full = tools.checkIfFull(roomID);
        setEquipmentLayout();
        returnButton.addActionListener(this);
        showEquipments.addActionListener(this);
        showWindows.addActionListener(this);
        mapping.addActionListener(this);
        removeEquipments.addActionListener(this);
    }

    public void setEquipmentLayout() {
        generalPanel = new JPanel(new BorderLayout());
        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel(new GridBagLayout());
        panel1.setLayout(new GridLayout(width,length));
        panel1.setBorder(new TitledBorder("Plan de la salle ID " + roomID + ", " + floor_name + ", " + b_name));

        // plan of the room
        for(int i=0; i<width; i++) {
            for(int j=0; j<length; j++) {
                cells1[i][j] = new MySpot(i,j,"c");
                cells1[i][j].addActionListener(this);
                cells1[i][j].setBackground(Color.gray);
                cells1[i][j].setEnabled(false);
            }
        }
       
        for(int i=0; i<width; i++) {
            for(int j=0; j<length; j++) {
                panel1.add(cells1[i][j]);
            }
        }

        List<String> list_x_equipment, list_y_equipment, list_state_equipment, list_id_equipment, list_name_equipment;
        List<Integer> list_x_free, list_y_free;

        if(!empty) {
            HashMap<String,List<String>> occupied = tools.getOccupiedSpace(roomID);
            list_x_equipment = occupied.get("x");
            list_y_equipment = occupied.get("y");
            list_state_equipment = occupied.get("state");
            list_id_equipment = occupied.get("id");
            list_name_equipment = occupied.get("name");
            for(int i=0; i<list_x_equipment.size(); i++) {
                int x = Integer.parseInt(list_x_equipment.get(i));
                int y = Integer.parseInt(list_y_equipment.get(i));
                if(list_state_equipment.get(i).equals("true")) {
                    cells1[x][y].setBackground(Color.green);
                    cells1[x][y].setTag("t");
                }
                else {
                    cells1[x][y].setBackground(Color.red);
                    cells1[x][y].setTag("f");
                }
                String txt = list_name_equipment.get(i) + " " + list_id_equipment.get(i);
                cells1[x][y].setID(list_id_equipment.get(i));
                cells1[x][y].setText(txt);
                cells1[x][y].setName(list_name_equipment.get(i));
                cells1[x][y].setEnabled(true);
            }
        }

        if(!full) {
            HashMap<String,List<Integer>> free = tools.getFreeSpace(roomID);
            list_x_free = free.get("x");
            list_y_free = free.get("y");
            for(int i=0; i<list_x_free.size(); i++) {
                cells1[list_x_free.get(i)][list_y_free.get(i)].setBackground(Color.yellow);
                cells1[list_x_free.get(i)][list_y_free.get(i)].setTag("d");
            }
        }

        generalPanel.add(panel1, BorderLayout.CENTER);
        generalPanel.add(l1, BorderLayout.NORTH);

        JPanel buttonsPanel = new JPanel(new GridLayout(0, 1, 30, 60));
        buttonsPanel.add(showWindows);
        buttonsPanel.add(mapping);
        buttonsPanel.add(removeEquipments);
        panel3.add(buttonsPanel);
        if(!full && !empty){
            mapping.setEnabled(true);
            removeEquipments.setEnabled(true);
        }
        else if(full){
            mapping.setEnabled(false);
            removeEquipments.setEnabled(true);
        }
        else if(empty){
            mapping.setEnabled(true);
            removeEquipments.setEnabled(false);
        }
        
        
        generalPanel.add(panel3, BorderLayout.LINE_END);

        panel2.add(returnButton);
        generalPanel.add(panel2, BorderLayout.PAGE_END);
        this.setContentPane(generalPanel);
    }

    public void setWindowLayout() {
        generalPanel = new JPanel(new BorderLayout());
        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel(new BorderLayout());
        panel1.setLayout(new GridLayout(width,length));
        panel1.setBorder(new TitledBorder("Plan de la salle ID " + roomID + ", " + floor_name + ", " + b_name));
        List<String> list_x_window, list_y_window, list_state_window, list_id_window;
        HashMap<String,List<String>> windows_list = tools.getWindows(roomID);
        list_id_window = windows_list.get("id");
        list_x_window = windows_list.get("x");
        list_y_window = windows_list.get("y");
        list_state_window = windows_list.get("state");
        for(int i=0; i<width; i++) {
            for(int j=0; j<length; j++) {
                cells2[i][j] = new MySpot(i,j,"c");
                cells2[i][j].setBackground(Color.gray);
                cells2[i][j].setEnabled(false);
                cells2[i][j].addActionListener(this);
                panel1.add(cells2[i][j]);
            }
        }
        for(int i=0; i<list_x_window.size(); i++) {
            int x = Integer.parseInt(list_x_window.get(i));
            int y = Integer.parseInt(list_y_window.get(i));
            if(list_state_window.get(i).equals("true")) {
                cells2[x][y].setBackground(Color.green);
                cells2[x][y].setTag("t");
            }
            else {
                cells2[x][y].setBackground(Color.red);
                cells2[x][y].setTag("f");
            }
            String txt = "Fenetre " + list_id_window.get(i) ;
            cells2[x][y].setID(list_id_window.get(i));
            cells2[x][y].setText(txt);
            cells2[x][y].setName("window");
            cells2[x][y].setEnabled(true);
        }
        generalPanel.add(panel1, BorderLayout.CENTER);

        generalPanel.add(l2, BorderLayout.NORTH);

        panel3.add(showEquipments, BorderLayout.SOUTH);
        generalPanel.add(panel3, BorderLayout.LINE_END);

        panel2.add(returnButton);
        generalPanel.add(panel2, BorderLayout.PAGE_END);
        this.setContentPane(generalPanel);
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == showWindows) {
            this.setWindowLayout();
            this.revalidate();
        }
        else if(e.getSource() == showEquipments) {
            this.setEquipmentLayout();
            this.revalidate();
        }
        else if(e.getSource() instanceof MySpot) {
            MySpot p = (MySpot)e.getSource();
            if(p.getName()=="window"){
                updateWindowState(p);
            }
            else {
                updateEquipmentState(p);
            }
        }
        else if(e.getSource() == mapping) {
            MappingInterface mi = new MappingInterface(this, tools, roomID, cells1);
            this.setVisible(false);
            mi.showWindow();
        }
        else if(e.getSource() == removeEquipments) {
            RemoveInterface ri = new RemoveInterface(this, tools, cells1);
            this.setVisible(false);
            ri.showWindow();
        } 
        else if(e.getSource() == returnButton) {
            previous.setVisible(true);
            this.dispose();
        }
    }

    public void updateWindowState(MySpot s){
        if(s.getTag()=="t"){
            tools.updateWindowState(Integer.parseInt(s.getID()), "false");
            s.setTag("f");
            s.setBackground(Color.red);
        }
        else {
            tools.updateWindowState(Integer.parseInt(s.getID()), "true");
            s.setTag("t");
            s.setBackground(Color.green);
        }
    }

    public void updateEquipmentState(MySpot s){
        if(s.getTag()=="t"){
            tools.updateState(Integer.parseInt(s.getID()), "false");
            s.setTag("f");
            s.setBackground(Color.red);
        }
        else {
            tools.updateState(Integer.parseInt(s.getID()), "true");
            s.setTag("t");
            s.setBackground(Color.green);
        }
    }

    public void updateNewlyMappedEquipment(int id, int x, int y, String name){
        cells1[x][y].setTag("t");
        cells1[x][y].setID(String.valueOf(id));
        cells1[x][y].setBackground(Color.green);
        String txt = name + " " + id;
        cells1[x][y].setText(txt);
        cells1[x][y].setName(name);
        cells1[x][y].setEnabled(true);
        empty = false;
        full = tools.checkIfFull(roomID);
        if(!full && !empty){
            mapping.setEnabled(true);
            removeEquipments.setEnabled(true);
        }
        else if(full){
            mapping.setEnabled(false);
            removeEquipments.setEnabled(true);
        }
    }

    public void updateNewlyRemovedEquipment(int x, int y) {
        cells1[x][y].setTag("d");
        cells1[x][y].setID("-1");
        cells1[x][y].setName("null");
        cells1[x][y].setText("");
        cells1[x][y].setBackground(Color.yellow);
        cells1[x][y].setEnabled(false);
        empty = tools.checkEmptyRoom(roomID);
        full = false;
        if(!full && !empty){
            mapping.setEnabled(true);
            removeEquipments.setEnabled(true);
        }
        else if(empty){
            mapping.setEnabled(true);
            removeEquipments.setEnabled(false);
        }
    }

    public void showWindow() {
        this.pack();
        this.setSize(900,600);
        this.setLocationRelativeTo(null);
        this.setTitle("");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(true);
        this.setVisible(true);
    }
}
