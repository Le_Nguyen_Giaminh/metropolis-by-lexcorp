package mapping.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import edu.episen.si.ing1.pds.metropolis.client.ClientConfig;
import edu.episen.si.ing1.pds.metropolis.client.ClientCore;
import mapping.tools.FloorTools;
import mapping.tools.RoomTools;
import mapping.windows.BigMeetingRoom;
import mapping.windows.Floor;
import mapping.windows.IndividualSpace;
import mapping.windows.OpenSpace;
import mapping.windows.SmallMeetingRoom;

public class TestMapping {
    public static void main(String[] args) throws IOException{
        ClientConfig config = new ClientConfig();
        ClientCore client = new ClientCore(config);
        RoomTools smr = new RoomTools(client);
        FloorTools fl = new FloorTools(client);
        //SmallMeetingRoom w = new SmallMeetingRoom(smr,1);
        //w.showWindow();

        //Floor fw = new Floor(fl,1,"Tesla");
        //fw.showWindow();
        
        //BigMRWindow w = new BigMRWindow(smr,2);
        //w.showWindow();
        //OpenSpaceWindow w = new OpenSpaceWindow(smr,3);
        //w.showWindow();
        //IndividualSpaceWindow w = new IndividualSpaceWindow(smr,37);
        //w.showWindow();
    }
}
