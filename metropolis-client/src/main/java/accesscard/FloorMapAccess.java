package accesscard;


import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.episen.si.ing1.pds.metropolis.client.ClientConfig;
import edu.episen.si.ing1.pds.metropolis.client.ClientCore;
import main_page.Accueil;
import sun.java2d.loops.DrawGlyphListAA.General;

public class FloorMapAccess implements ActionListener, ItemListener {
	private ClientCore client;
	JFrame fp=new JFrame();
	private JPanel generalPanel = new JPanel(new BorderLayout());
	private JPanel panel1 = new JPanel();
	private JLabel bulding = new JLabel("Batiment :");
	private String[] numbat = { "A", "B", "C", "D" };
	private JComboBox buildingChoice = new JComboBox(numbat);
	private JLabel floor = new JLabel("Etage :");
	private String[] numet = { "1", "2", "3" };
	private JComboBox floorChoice = new JComboBox(numet);
	private JButton validate = new JButton("Valider");
	private String response2;
	private String response3;
	private String response4;
	private String response5;
	private String response6;
	private String response7;
	private String response8;
	private JPanel genPan=new JPanel();
	private JPanel panel2;
	private JPanel panel3 = new JPanel();
	private JButton returnButton = new JButton("retour"); 
	private JButton validateSelected = new JButton("Valider"); 
	private ArrayList<JButton> eiName= new ArrayList<JButton>();
	private ArrayList<String> NoName= new ArrayList<String>();
	private ArrayList<String> roomName= new ArrayList<String>();
	private ArrayList<String> PSDRName= new ArrayList<String>();
	private ArrayList<String> GSDRName= new ArrayList<String>();
	private ArrayList<String> OSName= new ArrayList<String>();
	private ArrayList<String> EINames= new ArrayList<String>();
	private ArrayList<String> PSDRNames= new ArrayList<String>();
	private ArrayList<String> GSDRNames= new ArrayList<String>();
	private ArrayList<String> OSNames= new ArrayList<String>();
	private HashMap<String,String> roomMap=new HashMap<String,String>();
	private HashMap<String,String> PSDRMap=new HashMap<String,String>();
	private HashMap<String,String> GSDRMap=new HashMap<String,String>();
	private HashMap<String,ArrayList<String>> res;
	int i=0;
	CardLayout cd=new CardLayout();
	public FloorMapAccess(ClientCore client) {
		this.client = client;
		generalPanel.setBorder(new TitledBorder("Choisissez vos accès "));
		fp.setPreferredSize(new Dimension(1000,600));
		fp.setVisible(true);
		fp.pack();
		fp.setLocationRelativeTo(null);
		fp.setMinimumSize(new Dimension(200,300));
		validate.setBackground(Color.blue);
		validateSelected.setBackground(Color.blue);
		returnButton.setBackground(Color.blue);
		// first panel
		panel1.setLayout(new GridLayout(1, 3));
		JPanel panel11 = new JPanel(new FlowLayout());
		panel11.add(bulding);
		panel11.add(buildingChoice);
		JPanel panel12 = new JPanel(new FlowLayout());
		panel12.add(floor);
		panel12.add(floorChoice);
		panel1.add(panel11);
		panel1.add(panel12);
		panel1.add(validate);
		generalPanel.add(panel1, BorderLayout.NORTH);
	

		// second panel
		/* JLabel lblNom = new javax.swing.JLabel();
	       JLabel lblPrenom = new javax.swing.JLabel();
	       lblNom.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
	        lblNom.setText("Nom  :");
	        lblPrenom.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
	        lblPrenom.setText("Prenom  :");
	  JTextField jTextField_LastName = new javax.swing.JTextField();
	  JTextField     jTextField_FirstName = new javax.swing.JTextField(); */
		
		/*panel2.add(jTextField_FirstName);
		panel2.add(jTextField_LastName); 
		panel2.add(lblNom);
		panel2.add(lblPrenom); */
		
		

		// third panel
		JLabel lb1 = new JLabel("EI : Espace individuel / ");
		JLabel lb2 = new JLabel("PSDR : Petite salle de réunion / ");
		JLabel lb3 = new JLabel("GSDR : Grande salle de réunion / ");
		JLabel lb4 = new JLabel("OS : Open space");
		panel3.add(lb1);
		panel3.add(lb2);
		panel3.add(lb3);
		panel3.add(lb4);
		panel3.add(returnButton);
		panel3.add(validateSelected);
		genPan.setLayout(cd);
		genPan.add("generalKHRA",generalPanel);
		cd.show(genPan, "generalKHRA");
		fp.getContentPane().add(genPan);
		//TEST1
		returnButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				//generalPanel = new JPanel(new BorderLayout());
			
				for(int m=0;m<eiName.size();m++) {
					if(eiName.get(m).getBackground()!=Color.blue) {
						eiName.get(m).setEnabled(false);
					}
					
					
				}  
				FloorMapAccess newmap=new FloorMapAccess(client); 
				fp.dispose();

        		
			}
			
		});
		
		
		//test1
		validate.addActionListener(new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Veuillez patienter quelques secondes..");
				panel2 = planFloor();

				JPanel panel99=new JPanel(new BorderLayout());
				panel99.add(panel3, BorderLayout.SOUTH);
				panel99.add(panel2, BorderLayout.CENTER);
				genPan.add(""+i+"",panel99);
				cd.show(genPan, ""+i+"");
				i++;
				 
				String rqUpd="update badge SET state = true WHERE names = '"+AddCardToEmployee.getjTextField_FirstName().getText()+"'";
				String rqUpd1="update associate set badge_id=(select badge_id from badge where names='"+AddCardToEmployee.getjTextField_FirstName().getText()+"') where employee_id=(select employee_id from employee where name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"')";
				client.sendRequest(rqUpd1);
				client.sendRequest(rqUpd);
				String sqlDisableTrigger="alter table Badge disable trigger all";
				client.sendRequest(sqlDisableTrigger);
		        String rqUpdateBadgeid="update employee set badgeid=(select badge_id from badge where names='"+AddCardToEmployee.getjTextField_FirstName().getText()+"') where name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'";
		        client.sendRequest(rqUpdateBadgeid);
		        String sqlEnableTrigger ="ALTER TABLE badge ENABLE TRIGGER ALL";
		        client.sendRequest(sqlEnableTrigger);

		 }
		});
		validateSelected.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Attribué avec succés");
				for(int i=0;i<EINames.size();i++) {
				String rqIns =	"insert into public.associate ( roomid, employee_id, state,badge_id)"+
			            "VALUES ((select room_id FROM room WHERE r_name='"+EINames.get(i)+ "' and floor_id = (select floor_id FROM floors WHERE f_name ='Etage "
				        + floorChoice.getSelectedItem() + "' AND build_id= (select build_id FROM building WHERE b_name ='Batiment "
						+ buildingChoice.getSelectedItem()+ "')"
			            +  ")),(select employee_id FROM employee WHERE employee.lastname='"
				        +AddCardToEmployee.getjTextField_LastName().getText()+"' AND employee.name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'), true,(select badge_id from badge where badge.names='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'))"; 
						client.sendRequest(rqIns);
				}
				for(int i=0;i<PSDRNames.size();i++) {
				
					String rqIns =	"insert into public.associate ( roomid, employee_id, state,badge_id)"+
				            "VALUES ((select room_id FROM room WHERE r_name='"+PSDRNames.get(i)+ "' and floor_id = (SELECT floor_id FROM floors WHERE f_name ='Etage "
					        + floorChoice.getSelectedItem() + "' AND build_id= (select build_id FROM building WHERE b_name ='Batiment "
							        + buildingChoice.getSelectedItem()+ "')"
				            +  ")),(select employee_id FROM employee WHERE employee.lastname='"
					        +AddCardToEmployee.getjTextField_LastName().getText()+"' AND employee.name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'),true, (select badge_id from badge where badge.names='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'))"; 
							client.sendRequest(rqIns);
							
					}
				for(int i=0;i<OSNames.size();i++) {
					
					String rqIns =	"insert into public.associate ( roomid, employee_id, state,badge_id)"+
				            "VALUES ((select room_id FROM room WHERE r_name='"+OSNames.get(i)+ "' and floor_id = (SELECT floor_id FROM floors WHERE f_name ='Etage "
					        + floorChoice.getSelectedItem() + "' AND build_id= (select build_id FROM building WHERE b_name ='Batiment "
							        + buildingChoice.getSelectedItem()+ "')"
				            +  ")),(select employee_id FROM employee WHERE employee.lastname='"
					        +AddCardToEmployee.getjTextField_LastName().getText()+"' AND employee.name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'), true,(select badge_id from badge where badge.names='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'))"; 
							client.sendRequest(rqIns);
							
					}
	     for(int i=0;i<GSDRNames.size();i++) {
					
					String rqIns =	"insert into public.associate ( roomid, employee_id, state,badge_id)"+
				            "VALUES ((select room_id FROM room WHERE r_name='"+GSDRNames.get(i)+ "' and floor_id = (SELECT floor_id FROM floors WHERE f_name ='Etage "
					        + floorChoice.getSelectedItem() + "' AND build_id= (select build_id FROM building WHERE b_name ='Batiment "
							        + buildingChoice.getSelectedItem()+ "')"
				            +  ")),(select employee_id FROM employee WHERE employee.lastname='"
					        +AddCardToEmployee.getjTextField_LastName().getText()+"' AND employee.name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'), true,(select badge_id from badge where badge.names='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'))"; 
							client.sendRequest(rqIns);
							
					}
	     //if badge de qqun vide insert nom prenom code 
				String rqUpd="update badge SET state = true WHERE names = '"+AddCardToEmployee.getjTextField_FirstName().getText()+"'";
				client.sendRequest(rqUpd);
				String sqlDisableTrigger="alter table Badge disable trigger all";
				client.sendRequest(sqlDisableTrigger);
		        String rqUpdateBadgeid="update employee set badgeid=(select badge_id from badge where names='"+AddCardToEmployee.getjTextField_FirstName().getText()+"') where name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"'";
		        client.sendRequest(rqUpdateBadgeid);
		        String sqlEnableTrigger ="ALTER TABLE badge ENABLE TRIGGER ALL";
		        client.sendRequest(sqlEnableTrigger);
		       
						
			}
			
		});
		
	}

	public JPanel planFloor() {
		JPanel p = new JPanel(new GridLayout(7, 15));
		JPanel[][] colorRoom = new JPanel[7][15];
		Border lineBorder = BorderFactory.createLineBorder(Color.BLACK, 1);
		p.setBorder(lineBorder);
		try {	ClientConfig cng = new ClientConfig();
		ClientConfig cng1= new ClientConfig();
		ClientCore cl= new ClientCore(cng);
		ClientCore cl1= new ClientCore(cng1);

       String sqlRequest="select * from room inner join reserve on room.room_id=reserve.room_id "+
		" inner join floors on room.floor_id=floors.floor_id"+
	   " inner join building on floors.build_id=building.build_id"+
	 " inner join company on reserve.comp_id=company.comp_id where c_name='"+Accueil.getComboBox().getSelectedItem().toString()+"'"+
					" and b_name='Batiment "+ buildingChoice.getSelectedItem()+ 
					
					"' and f_name='Etage "+ floorChoice.getSelectedItem()+ "' and valid = true and available= false ";
		String response = cl.sendRequest(sqlRequest).getResponseBody();
		
	
		String[] values=response.split("date_reserv");
		
		String roomNameValue=null;
		String availability=null;
		for(int i=0;i<values.length;i++) {
			if(values[i].contains("r_name")) {
				String[] values1=values[i].split(",");
				for(int j=0;j<values1.length;j++) {
					if(values1[j].contains("r_name")){
						roomNameValue=(values1[j].split("="))[1];
						roomName.add(roomNameValue);
						
					}
				}
			}
		}
		
	
	}catch(Exception e) {
		e.printStackTrace();
	}
		
		//boucle to optimize
		for (int i = 0; i <= 1; i++) { // individual spaces
			for (int j = 0; j <= 9; j++) {
				String EI1="EI";
				JButton EI = new JButton(EI1);
				if(i==0) {
					int k=j+1;
					EI.setText(EI1 +k);//&& (!response2.isEmpty())
					if(roomName.contains("EI"+k) && verifyAlreadyAccess(k)==true) {
						 EI.setBackground(Color.green);}
						 else {EI.setBackground(Color.lightGray);
							EI.setEnabled(false);
							
							 
						 }
						
					}
			
				if(i==1) {
					int k= j+11;
				EI.setText(EI1 + k);//&& (!response2.isEmpty())
				if(roomName.contains("EI"+k)&& verifyAlreadyAccess(k)==true){
		
					 EI.setBackground(Color.green);}
				 else {EI.setBackground(Color.lightGray);
					EI.setEnabled(false);
				
					
					
				}
				}
				
			
			
				colorRoom[i][j] = new JPanel();
				colorRoom[i][j].setBackground(new Color(180, 128, 177));
				colorRoom[i][j].setBorder(lineBorder);
				colorRoom[i][j].add(EI);
		
				
				EI.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						if(EI.getBackground()==Color.GREEN) {
							EI.setBackground(Color.MAGENTA);
							EINames.add(EI.getText());
						}
						else {
							EI.setBackground(Color.GREEN);
							EINames.remove(EI.getText());
						} 
					}
					
				});
				
			
		    }
		}

		try {	ClientConfig cng = new ClientConfig();
		ClientCore cl= new ClientCore(cng);
		String sqlRequest="select * from room inner join reserve on room.room_id=reserve.room_id"+
				" inner join floors on room.floor_id=floors.floor_id"+
				   " inner join building on floors.build_id=building.build_id"+
				 " inner join company on reserve.comp_id=company.comp_id where c_name='"+Accueil.getComboBox().getSelectedItem().toString()+"'"+
								" and b_name='Batiment "+ buildingChoice.getSelectedItem()+ "' and f_name='Etage "+ floorChoice.getSelectedItem()+ "' and valid = true and available= false ";
	String sqlRequest3="select b_name, r_name,type, size, r.capacity, f_name  from room r "+  
				"inner join associate A on  A.roomid = r.room_id "+
				"inner join employee C ON A.employee_id=(select employee_id from employee where employee.name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"')"
				+ "inner join floors f  on r.floor_id = f.floor_id  inner join building b  on f.build_id = b.build_id where r.available = false and type= 'PSDR' "
				+" and b_name='Batiment "+buildingChoice.getSelectedItem()+"' and f_name= 'Etage "+ floorChoice.getSelectedItem()+"'";	
		
		
		String response = cl.sendRequest(sqlRequest).getResponseBody();
		response3=cl.sendRequest(sqlRequest3).getResponseBody();
		System.out.println(response);
		String[] values=response.split("date_reserv");
		String roomNameValue=null;
		String availability=null;
		for(int i=0;i<values.length;i++) {
			if(values[i].contains("r_name")) {
				String[] values1=values[i].split(",");
				for(int j=0;j<values1.length;j++) {
					if(values1[j].contains("r_name")){
						roomNameValue=(values1[j].split("="))[1];
						PSDRName.add(roomNameValue);
					}
				}
			}
		
		}
	System.out.println(roomName.toString());
	}catch(Exception e) {
		e.printStackTrace();
	}

		for (int i = 3; i <= 6; i++) { // small meeting room
			for (int j = 0; j <= 3; j++) {
				colorRoom[i][j] = new JPanel();
				colorRoom[i][j].setBackground(new Color(122, 206, 93));
			}
		}
		JButton PSDR = new JButton("PSDR");//&& (!response3.isBlank()
		if(PSDRName.contains("PSDR" )&& response3.isEmpty())  {
			
			  PSDR.setBackground(Color.green);
			}
		
		else {	PSDR.setBackground(Color.lightGray);
				PSDR.setEnabled(false);}
			
	
	
		PSDR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(PSDR.getBackground()==Color.GREEN) {
					PSDR.setBackground(Color.MAGENTA);
					PSDRNames.add(PSDR.getText());

				}
				else {
					PSDR.setBackground(Color.GREEN);
					PSDRNames.remove(PSDR.getText());
				} 
				
			}
		});
colorRoom[4][1].add(PSDR);


try {	
	
ClientConfig cng = new ClientConfig();
ClientCore cl= new ClientCore(cng);

String sqlRequest="select * from room inner join reserve on room.room_id=reserve.room_id"+
		" inner join floors on room.floor_id=floors.floor_id"+
		   " inner join building on floors.build_id=building.build_id"+
		 " inner join company on reserve.comp_id=company.comp_id where c_name='"+Accueil.getComboBox().getSelectedItem().toString()+"'"+
						" and b_name='Batiment "+ buildingChoice.getSelectedItem()+ "' and f_name='Etage "+ floorChoice.getSelectedItem()+ "' and valid = true and available= false and type='GSDR' ";
String sqlRequest4="select b_name, r_name,type, size, r.capacity, f_name  from room r "+  
		"inner join associate A on  A.roomid = r.room_id "+
		"inner join employee C ON A.employee_id=(select employee_id from employee where employee.name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"')"
		+ "inner join floors f  on r.floor_id = f.floor_id  inner join building b  on f.build_id = b.build_id "
		+" where r.available = false and type= 'GSDR' and b_name='Batiment "+buildingChoice.getSelectedItem()+"' and "+
		" f_name='Etage "+floorChoice.getSelectedItem()+"'";	
			
response4=cl.sendRequest(sqlRequest4).getResponseBody();	
String response = cl.sendRequest(sqlRequest).getResponseBody();

String[] values=response.split("date_reserv");

String roomNameValue=null;
for(int i=0;i<values.length;i++) {
	if(values[i].contains("r_name")) {
		String[] values1=values[i].split(",");
		for(int j=0;j<values1.length;j++) {
			if(values1[j].contains("r_name")){
				roomNameValue=(values1[j].split("="))[1];
				GSDRName.add(roomNameValue);
			}
		}
	}
}

}catch(Exception e) {
e.printStackTrace();
}



		for (int i = 3; i <= 6; i++) { // large meeting room
			for (int j = 4; j <= 9; j++) {
				colorRoom[i][j] = new JPanel();
				colorRoom[i][j].setBackground(new Color(237, 229, 113));
			}
		}
		JButton GSDR = new JButton("GSDR");//&& (!response4.isEmpty())
		if(GSDRName.contains("GSDR") && response4.isEmpty()) {
			
			  GSDR.setBackground(Color.green);
			}
		
		else {	GSDR.setBackground(Color.lightGray);
				GSDR.setEnabled(false);}
		GSDR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(GSDR.getBackground()==Color.GREEN) {
					GSDR.setBackground(Color.MAGENTA);
					GSDRNames.add(GSDR.getText());

				}
				else {
					GSDR.setBackground(Color.GREEN);
					GSDRNames.remove(GSDR.getText());
				} 
				
			}
		});
		colorRoom[4][6].add(GSDR);
		
		try {	ClientConfig cng = new ClientConfig();
		ClientCore cl= new ClientCore(cng);

		String sqlRequest="select * from room inner join reserve on room.room_id=reserve.room_id"+
				" inner join floors on room.floor_id=floors.floor_id"+
			    " inner join building on floors.build_id=building.build_id"+
				 " inner join company on reserve.comp_id=company.comp_id where c_name='"+Accueil.getComboBox().getSelectedItem().toString()+"'"+
				 " and b_name='Batiment "+ buildingChoice.getSelectedItem()+ "' and f_name='Etage "+ floorChoice.getSelectedItem()+ "' and valid = true and available= false and type='OS' ";
		String sqlRequest5="select b_name, r_name,type, size, r.capacity, f_name  from room r "+  
				"inner join associate A on  A.roomid = r.room_id "+
				"inner join employee C ON A.employee_id=(select employee_id from employee where employee.name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"')"
				+ "inner join floors f  on r.floor_id = f.floor_id  inner join building b  on f.build_id = b.build_id where r.available = false and type= 'OS' "+
				" and b_name='Batiment "+buildingChoice.getSelectedItem()+"' and f_name='Etage "
				+ floorChoice.getSelectedItem()+"'";	
							
		response5=cl.sendRequest(sqlRequest5).getResponseBody();	
		String response = cl.sendRequest(sqlRequest).getResponseBody();

		String[] values=response.split("date_reserv");

		String roomNameValue=null;
		for(int i=0;i<values.length;i++) {
			if(values[i].contains("r_name")) {
				String[] values1=values[i].split(",");
				for(int j=0;j<values1.length;j++) {
					if(values1[j].contains("r_name")){
						roomNameValue=(values1[j].split("="))[1];
						OSName.add(roomNameValue);
					}
				}
			}
		}

		}catch(Exception e) {
		e.printStackTrace();
		}

		for (int i = 0; i <= 5; i++) { // open space
			for (int j = 11; j <= 14; j++) {
				colorRoom[i][j] = new JPanel();
				colorRoom[i][j].setBackground(new Color(46, 127, 245));
			}
		}
		JButton os = new JButton("OS");
		
		//&& (!response4.isEmpty())
		if(OSName.contains("OS")&& (response5.isEmpty())) {
			
			  os.setBackground(Color.green);
			}
		
		else {	os.setBackground(Color.lightGray);
				os.setEnabled(false);}
		os.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(os.getBackground()==Color.GREEN) {
					os.setBackground(Color.MAGENTA);
					OSNames.add(os.getText());

				}
				else {
					os.setBackground(Color.GREEN);
					OSNames.remove(os.getText());
				} 
				
			}
		});

		
	
		colorRoom[3][12].add(os);

		for (int j = 0; j <= 9; j++) { // corridor1
			colorRoom[2][j] = new JPanel();
			colorRoom[2][j].setBackground(Color.LIGHT_GRAY);
		}
		for (int i = 0; i <= 5; i++) { // corridor2
			colorRoom[i][10] = new JPanel();
			colorRoom[i][10].setBackground(Color.LIGHT_GRAY);
		}
		for (int j = 10; j <= 14; j++) { // corridor3
			colorRoom[6][j] = new JPanel();
			colorRoom[6][j].setBackground(Color.LIGHT_GRAY);
		}
		for (int i = 0; i <= 6; i++) { // add color to p
			for (int j = 0; j <= 14; j++) {
				p.add(colorRoom[i][j]);
			}
		}
		
		
		//test 
		try {	ClientConfig cng = new ClientConfig();
		ClientCore cl= new ClientCore(cng);
		String sqlRequest="select * from room inner join reserve on room.room_id=reserve.room_id"+
				" inner join floors on room.floor_id=floors.floor_id"+
				   " inner join building on floors.build_id=building.build_id"+
				 " inner join company on reserve.comp_id=company.comp_id where c_name='"+Accueil.getComboBox().getSelectedItem().toString()+"'"+
								" and b_name='Batiment "+ buildingChoice.getSelectedItem()+ "' and f_name='Etage "+ floorChoice.getSelectedItem()+ "' and valid = true and available= false ";
		String response = cl.sendRequest(sqlRequest).getResponseBody();
		
		System.out.println(response);
		String[] values=response.split("date_reserv");
		String roomNameValue=null;
		for(int i=0;i<values.length;i++) {
			if(values[i].contains("r_name")) {
				String[] values1=values[i].split(",");
				for(int j=0;j<values1.length;j++) {
					if(values1[j].contains("r_name")){
						roomNameValue=(values1[j].split("="))[1];
						NoName.add(roomNameValue);
					}
				}
			}
		}
	}catch(Exception e) {
		e.printStackTrace();
	}
		System.out.print("ok"+NoName);
		if( NoName.contains("")){
			JOptionPane.showMessageDialog(null, ""+Accueil.getComboBox().getSelectedItem().toString()+" n'a pas effectué de résérvations " );
		}
		return p;
	}

	public ArrayList<String> getRoomName() {
		return roomName;
	}
	public boolean verifyAlreadyAccess(int l) {
		String sqlRequest2="select b_name, r_name,type, size, r.capacity, f_name  from room r "+  
				"inner join associate A on  A.roomid = r.room_id "+
							"inner join employee C ON A.employee_id=(select employee_id from employee where employee.name='"+AddCardToEmployee.getjTextField_FirstName().getText()+"')"+
							 "inner join floors f  on r.floor_id = f.floor_id  inner join building b  on "
							+ " f.build_id = b.build_id where r.available = false and type= 'EI' and r_name='EI"+l+"'"
							+ " and b_name='Batiment "+buildingChoice.getSelectedItem()+"' and f_name='Etage "+floorChoice.getSelectedItem()+"'";	
					response2=client.sendRequest(sqlRequest2).getResponseBody();
		if(response2.isEmpty()) {
			return true;
		}
		else {			
		return false;}
		
	}
	public boolean verifyExistBadge() {
		ClientConfig cg = null;
		try {
			cg = new ClientConfig();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ClientCore cl =null;
		try {
			cl =new ClientCore(cg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		String sqlRequest1="select * from badge where names='"+AddCardToEmployee.getjTextField_FirstName()+"'";
		response6=cl.sendRequest(sqlRequest1).getResponseBody();
		if(response6.isEmpty()) {
			String sqlGenerate="select badge_name from badge";
			response7=cl.sendRequest(sqlGenerate).getResponseBody();
			res = new HashMap<String,ArrayList<String>>();
			String[] s1 = response7.split(",");
			ArrayList<String> list_badge = new ArrayList<>();
			System.out.println("eeeeeeeeeeeeeeeeeeeeeeee");
			for(int k=0; k<s1.length; k++) {
		            String[] s2 = s1[k].split("=");
		            if(s2[0].equals("badge_name")) {
		                list_badge.add(s2[1]);}
		    res.put("Badgename", list_badge); 
		    for (int l=0;l<list_badge.size();l++) {
	      	   res.get("Badgename").get(l).replaceAll("\\s","");
	         }
		    }
			System.out.println(list_badge.size());
			int nv= list_badge.size()+1000;
			String sqlInsertBadgeGene="INSERT INTO badge (badge_name,company_id,names,lastnames) VALUES ('EAN-UCC-"+nv+"',"+"(select comp_id from company where c_name='"+Accueil.getComboBox().getSelectedItem().toString()+"'),"+"'"+AddCardToEmployee.getjTextField_FirstName().getText()+"',"+"'"+AddCardToEmployee.getjTextField_LastName().getText()+"'"+")";
			response7=cl.sendRequest(sqlInsertBadgeGene).responseBody;
			JOptionPane.showMessageDialog(null,"Cet employé n'a pas encore de carte d'accés nous le lui attribuons une nouvelle carte suivante:"+"EAN-UCC-"+nv);
			return true;
		}
				
				
		return true;
	}
	

	

	@Override
	public void itemStateChanged(ItemEvent e) {

	}



	public static void main(String[] args) throws IOException {
		ClientConfig config = new ClientConfig();
		ClientCore client = new ClientCore(config);
		FloorMapAccess fw = new FloorMapAccess(client);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
	}
}

