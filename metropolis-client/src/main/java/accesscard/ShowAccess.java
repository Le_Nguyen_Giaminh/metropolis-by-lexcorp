package accesscard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import edu.episen.si.ing1.pds.metropolis.client.ClientConfig;
import edu.episen.si.ing1.pds.metropolis.client.ClientCore;
import main_page.Accueil;

public class ShowAccess extends javax.swing.JFrame {
	
	private ClientCore client;
 public ShowAccess() {
	 	Accueil.getMf().setEnabled(false);
        initComponents();
       
    }
   
 
                         
    public javax.swing.JTable getjTable2() {
	return jTable2;
}



public void setjTable2(javax.swing.JTable jTable2) {
	this.jTable2 = jTable2;
}



	private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelNameEmployee = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButtonCancel = new javax.swing.JButton();

      

        jPanel1.setBackground(new java.awt.Color(51, 153, 255));

        jLabelNameEmployee.setFont(new java.awt.Font("Tahoma", 1, 14)); // 
        ClientConfig cng;
		try {
			cng = new ClientConfig();
			ClientCore cl= new ClientCore(cng);
	        String sqlRequest="select badge_name from badge where names='"+ ShowAccessCard.getjTextField_FirstName().getText()+"'";
	        String response = cl.sendRequest(sqlRequest).getResponseBody();
	        String[] responseSplit= response.split("=");
	       
	        String code1= responseSplit[1].replaceAll("\\s","").replace(",","");
	        System.out.println(code1);
	        if(!(code1=="")) {
	        	
	            jLabelNameEmployee.setText("La carte d'accès de <"+code1+">");
	            }
	        else {
	            jLabelNameEmployee.setText("Les accès de la carte");
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
        
       

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Bâtiment", "Type", "Capacité", "Size", "Etage"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jButtonCancel.setBackground(new java.awt.Color(255, 255, 255));
        jButtonCancel.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        jButtonCancel.setText("Fermer");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addComponent(jLabelNameEmployee))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(152, 152, 152)
                        .addComponent(jButtonCancel)))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabelNameEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addComponent(jButtonCancel)
                .addGap(28, 28, 28))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 47, Short.MAX_VALUE))
        );

        pack();
    }


    private String setJLabelNameEmployee(String code2) {
		// TODO Auto-generated method stub
		return null;
	}



	public javax.swing.JLabel getjLabelNameEmployee() {
		return jLabelNameEmployee;
	}



	public void setjLabelNameEmployee(String code) {
		this.code=code;
	}



	private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {  
    	Accueil.getMf().setEnabled(true);
    	dispose();
    }  
    
    
    
    // fill employee 
    public void fillEmployeeCardTable(JTable table) {
    try {	ClientConfig cng = new ClientConfig();
	ClientCore cl= new ClientCore(cng);
	String sqlRequest= "select z.names, z.lastnames, z.badge_name, b_name, r_name, size, r.capacity, f_name"+
	" from room r"+
	" inner join associate A  on A.roomid = r.room_id"+
	" inner join reserve s on r.room_id= s.room_id "+
	" inner join badge z on A.employee_id=z.employee_id "+
	" inner join floors f on r.floor_id= f.floor_id "+
	" inner join building b on f.build_id= b.build_id "+
	" where r.available = false and z.state = true and s.valid = true and z.names ='"+ShowAccessCard.getjTextField_FirstName().getText()+"'";
	String response = cl.sendRequest(sqlRequest).getResponseBody();
	Object[] row= new Object[5];
	DefaultTableModel model =(DefaultTableModel)table.getModel();
	HashMap<String,List<String>> res = new HashMap<>();
	String[] s1 = response.split(",");
     List<String> list_name_b = new ArrayList<>();
     List<String> list_size = new ArrayList<>();
     List<String> list_capacity = new ArrayList<>();
     List<String> list_floor_num = new ArrayList<>();
     List<String> list_type = new ArrayList<>();
     for(int i=0; i<s1.length; i++) {
         String[] s2 = s1[i].split("=");
         if(s2[0].equals("b_name")) {
             list_size.add(s2[1]);
             row[0]=s2[1];
             //System.out.println(row[0].toString());
             
         }
         
         else if (s2[0].equals("r_name")){
             list_capacity.add(s2[1]);
             row[1]=s2[1];
             
            // System.out.println(row[1].toString());
           
         }
       
         else if (s2[0].equals("capacity")){
             list_name_b.add(s2[1]);
             row[2]=s2[1];
             //System.out.println(row[2].toString());
            
         }
         else if (s2[0].equals("size")){
             list_type.add(s2[1]);
             row[3]=s2[1];
           //  System.out.println(row[3].toString());
             
            
         }
         else if (s2[0].equals("f_name")){
             list_floor_num.add(s2[1]);
             row[4]=s2[1];
             //System.out.println(row[4].toString());
             model.addRow(row);
         }
         
        
     }
   
     res.put("Building", list_name_b);
     res.put("Type", list_type);
     res.put("Size", list_size);
     res.put("Capacity", list_capacity);
     res.put("Floor", list_floor_num);

     //model.addRow(row);
     
	
}catch(Exception e) {
	e.printStackTrace();
}
    
}


    public static void main(String args[]) {
     
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ShowAccessCard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ShowAccessCard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ShowAccessCard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ShowAccessCard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ShowAccessCard().setVisible(true);
            }
        });
    }

    private String code;             
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JLabel jLabelNameEmployee;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable2;
                
}